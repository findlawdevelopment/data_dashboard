# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='Data Dashboard||Jarvis',
    version='0.0.1',
    description='Tool to visualize Mobile WIP.',
    long_description=readme,
    author='Nate Reif-Wenner, Ken Korth, Brian Rogstad',
    author_email='nate.reif-wenner@thomsonreuters.com',
    url='http://domain.com',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
