import xlrd
# fix encoding issue
from datetime import datetime
from collections import OrderedDict
import simplejson as json
import os

def data_exists():
	if(hasattr(__data, 'sheet_names')):
		return true

class DataExtractor(object):

	def __init__(self, workbook_name):
		self.workbook_name = workbook_name
		self.__data = xlrd.open_workbook(self.workbook_name, formatting_info=False)
		self.date_mod = os.path.getmtime(workbook_name)
		self.target_sheet= self.__data.sheet_by_index(0)

	def get_workbook(self):
		sheet_names = __data.sheet_names
		return sheet_names
	
	def workbook_extract(self):
	#loop through all available sheets
		workbook_content_list = []
		column_header = []
		for rownum in range(self.target_sheet.nrows):
			row_list = self.target_sheet.row_values(rownum)
			if row_list[0] != "":
				workbook_content_list.append(row_list)
		return workbook_content_list
