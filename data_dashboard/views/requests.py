from flask import Flask 
from flask import render_template, g, session, request, Response, make_response, jsonify, redirect
from functools import update_wrapper
from flask.ext.mongoengine import MongoEngine
from flask.ext.login import LoginManager, login_user, logout_user, current_user, login_required
from mongoengine import Q
from apscheduler.scheduler import Scheduler
from data_dashboard.data_dump import output_data, saveThreshold
from data_dashboard import app
from data_dashboard.models.row_data import RowData, Thresholds, Product, Activity
from werkzeug.datastructures import Headers
from safeauth import SafeAuth
from data_dashboard.models.users import User
from data_dashboard.models.user_data import User_data
from data_dashboard.models.notes import Note
import re
import json
import pprint
import os
import platform
from datetime import datetime
import pytz
# import code for encoding urls and generating md5 hashes
import urllib.parse, hashlib

auth = SafeAuth(
    key='bru7REdeseSpAsad',

	safe_url='https://safeqa.thomson.com/login/sso/SSOService?app=traffik_qa'
)

@app.route('/get_date_modified', methods = ['GET'])


def ardwaag_location():
	if platform.system() == 'Linux':
		c = '/media/pub3008/General/Customer_Ops_Analytic_Reports/Dashboard_reports/ardwaag_ready_activities_rpt.csv'
	elif platform.system() == 'Windows':
		c = r'\\fl-custops-nas\pub3008\General\Customer_Ops_Analytic_Reports\Dashboard_reports\ardwaag_ready_activities_rpt.csv'
	t = os.path.getmtime(c)
	print(t)
	return str(t)


@app.route('/', methods = ['GET', 'POST'])
@app.route('/index', methods = ['GET', 'POST'])
def index():
	try:

		if session["user"]:
			email = session["user"]["email"]
			uid = session["user"]["safeID"]
			resp = make_response(render_template("index.html", user=email,uid = uid ))
			resp.set_cookie('test', 'returning visitor')
			return resp
	except Exception:

		if request.method == "POST":
			uid = request.form.get('uid', '')
			email = request.form.get('email', '')
			firstname = request.form.get('firstname', '')
			lastname = request.form.get('lastname', '')
			site_link = request.form.get('siteLink','')
			# if siteLink != "http://traffik_qa.int.thomson.com/":
			# 	forwarded_params= {'uid': uid, 'email':email, 'firstname':firstname, 'lastname':lastname, 'siteLink':siteLink}
			# 	return redirect("http://0.0.0.0:5000/",forwarded_form =forwarded_params )
			

			#Make sure SAFE authentication is valid
			if site_link != "http://0.0.0.0:5000":
				valid, message = auth.validate(
				uid=uid,
				time_string=request.form.get('time', ''),
				email=email,
				firstname=firstname,
				lastname=lastname,
				digest=request.form.get('digest', ''),
				)

				if not valid:
					return redirect('/forbidden')

			# Make sure user exists (or create one if not)

			User.objects(safe_id=uid).update_one(
			   set__safe_id=uid,
			   set__email=email,
			   set__first_name=firstname,
			   set__last_name=lastname,
			   upsert=True
			)
			user = User.objects(safe_id=uid).first()

			
			#make session permanent for 31 days (default)
			session.permanent = True

			session['user'] = {
			   'safeID': user.safe_id,
			   'email': user.email,
			   'firstName': user.first_name,
			   'lastName': user.last_name
			   
			}
			

			resp = make_response(render_template("index.html", user=email,uid = uid ))
			resp.set_cookie('test', 'test val')
			return resp
		else:
			return redirect(auth.safe_url + '&siteLink=http://' + request.headers['host'] + '/')

@app.route('/group_request/<group_call>', methods = ['GET', 'POST'])

def group_request(group_call):
	by_date = RowData.objects.order_by('-timestamp')
	most_recent = by_date[0].timestamp
	if re.search("\+", group_call):
		group_list = group_call.split("+")
		if len(group_list)==2:
			response = RowData.objects(Q(timestamp__gte=most_recent) & Q(org_unit=group_list[0]) | Q(timestamp__gte=most_recent) & Q(org_unit=group_list[1])).to_json()
			return response
		elif len(group_list)==3:
			response = RowData.objects(Q(timestamp__gte=most_recent) & Q(org_unit=group_list[0]) | Q(timestamp__gte=most_recent) & Q(org_unit=group_list[1]) | Q(timestamp__gte=most_recent) & Q(org_unit=group_list[2])).to_json()
			return response
		else:
			return "Error - more than three organizations requesteds"

	response = RowData.objects(Q(timestamp__gte=most_recent) & Q(org_unit=group_call)).to_json()
	return response




@app.route('/rows', methods=['GET'])
# @nocache
def service():
	by_date = RowData.objects.order_by('-timestamp')
	most_recent = by_date[0].timestamp
	response = RowData.objects(Q(timestamp__gte=most_recent) ).to_json()
	
	return response

@app.route('/thresholds', methods=['GET'])
# @nocache
def thresholds():
	
	response = Thresholds.objects().to_json()
	

	return response

# @app.route('/update_thresholds', methods = ['GET','POST'])

# def updateThresholds():
# 	getThresholds = request.get_json(force=True, cache=False)
# 	updateCurrentThresholds = Thresholds.objects(activity=getThresholds['activity']).update_one(upsert=True, **{
# 		"set__green_project": getThresholds['green_project'], 
# 		"set__yellow_project": getThresholds['yellow_project']
# 		})
# 	return getThresholds['activity'] + " successfully updated"

# @app.route('/save_new_threshold', methods = ['POST'])

# def addNewThreshold():
# 	try:
# 		getThresholds = request.get_json(force=True, cache=False)
# 		saveThreshold(getThresholds)
# 		return getThresholds['activity'] + " successfully added"
# 	except Exception:
# 		return "Please enter a valid threshold"

# @app.route('/remove_threshold', methods = ['POST'])

# def removeThreshold():
# 	removeThresholds = request.get_json(force=True, cache=False)
# 	removeCurrentThreshold = Thresholds.objects(activity=removeThresholds['activity']).delete();
# 	return removeThresholds['activity'] + " successfully removed"

@app.route('/save_new_threshold')
def addNewThreshold():
	threshold = Thresholds()
	threshold.process = "FirmSiteFulfillment"
	prod = Product()
	prod.product = "Firmsite III"
	act = Activity()
	act.activity = "Design Prototype Due"
	prod.activities.append(act)
	threshold.products.append(prod)
	threshold.save()
	return Thresholds.objects(process="FirmSiteFulfillment").to_json()

@app.route('/process_thresholds', methods = ['GET','POST'])
def process_thresholds():
	getSubmittedRows = request.get_json(force=True, cache=False)
	if len(getSubmittedRows) <1:
		return "no data received"

	for row in getSubmittedRows:
		print(row)
		submitted_process = row["process"]
		submitted_product = row["product"]
		submitted_activity = row["activity"]
		try:

			thresh = Thresholds.objects.get(label=submitted_process)
			print(thresh)
			found_product = False
			found_product_idx = None
			found_activity = False
			found_activity_idx = None
			for idx, product in enumerate(thresh.nodes):
				print (product.label)
				if product.label == submitted_product:
					
					print("the idx is")
					print(idx)
					found_product = True
					found_product_idx = idx
					break
			if not found_product:
				prod = Product()
				prod.label = submitted_product
				act = Activity()
				act.label = submitted_activity
				act.green_project = None
				act.yellow_project = None
				prod.nodes.append(act)
				thresh.nodes.append(prod)
				print("didn't find product")

			else:
				for a_idx, activity in enumerate(thresh.nodes[found_product_idx].nodes):
					if activity.label == submitted_activity:
						found_activity = True
						found_activity_idx = a_idx
						break
				if not found_activity:
					act = Activity()
					act.label = submitted_activity
					act.green_project = None
					act.yellow_project = None
					thresh.nodes[found_product_idx].nodes.append(act)
					print("found product")
				else:
					print("I think there was a problem")
					print("this product and activity exist")
			thresh.save()
			

		except Exception:
			print("couldn't find matching threshold")
			threshold = Thresholds()
			threshold.label = submitted_process
			prod = Product()
			prod.label = submitted_product
			act = Activity()
			act.label = submitted_activity
			act.green_project = None
			act.yellow_project = None
			prod.nodes.append(act)
			threshold.nodes.append(prod)
			threshold.save()
			
	return Thresholds.objects().to_json()




@app.route('/set_threshold', methods = ['GET','POST'])
def set_thresholds():
	thresh_data = request.get_json(force=True, cache=False)
	submitted_process = thresh_data["process"]
	submitted_product = thresh_data["product"]
	submitted_activity = thresh_data["activity"]
	submitted_green = thresh_data["green"]
	submitted_yellow = thresh_data["yellow"]
	product_idx = None
	activity_idx = None
	#find the right process
	qr = Thresholds.objects.get(label=submitted_process,nodes__label=submitted_product, nodes__nodes__label = submitted_activity)
	print(qr.to_json())
	for idx, product in enumerate(qr.nodes):
		print(submitted_product, product.label)
		if product.label == submitted_product:
			product_idx = idx
			print("found product match")
			break
		else:
			print("no match")
	
	if product_idx == None:
		print("no product match")
		return "no product match"
	for a_idx, activity in enumerate(qr.nodes[product_idx].nodes):
		if activity.label == submitted_activity:
			activity_idx = a_idx
			print("found activity match")
			break
		else:
			print("no activity match")
	if activity_idx == None:
		print("no activity match")
		return "no activity match"
	
	try:

		qr.nodes[product_idx].nodes[activity_idx].green_project = submitted_green
		qr.nodes[product_idx].nodes[activity_idx].yellow_project = submitted_yellow
		qr.save()
	except Exception:
		print("couldn't set green and yellow projects")
		return "save error"



	return Thresholds.objects().to_json()		
	


@app.route('/activity_list', methods = ['GET', 'POST'])

def getActivityList():
	getActivities = RowData.objects.distinct('activity_description')
	s = sorted(getActivities)
	print(s)
	activitiesList = json.dumps(s)
	return activitiesList

@app.route('/manager_list', methods = ['GET', 'POST'])

def getManagerList():
	getManagers = RowData.objects.distinct('staff_manager')
	s = sorted(getManagers)
	print(s)
	managerList = json.dumps(s)
	return managerList

@app.route('/ws', methods=['POST', 'GET'])
# @nocache
def ws():
	by_date = RowData.objects.order_by('-timestamp')
	most_recent = by_date[0].timestamp
	request_type = request.args.get('type', '')
	if request_type == "" or None:
		return '{"response":"no type value given"}'
	if request_type == "sub_id":
		sub_id = int(request.args.get('sub_id',''))
		response = RowData.objects(Q(timestamp__gte=most_recent) & Q(sub_id=sub_id)).to_json()
		return response
	if request_type == "participant":
		if request_type == "participant":
			participant_raw = request.args.get('participant', '')
			participant = participant_raw.split('_', 2)
			response = RowData.objects(Q(timestamp__gte=most_recent) & Q(participant_first_name=participant[0]) & Q(participant_last_name=participant[1])).to_json()
			print(participant)
			return response
		# participant_raw = request.args.get('participant', '')
		# participant = participant_raw.split(' ', 2)
		# if len(participant) == 2:
		# 	response = RowData.objects(Q(timestamp__gte=most_recent) & Q(participant_first_name=participant[0]) & Q(participant_last_name=participant[1])).to_json()
		# elif len(participant) == 3:
		# 	participant_last_array = participant[1] + ' ' + participant[2]
		# 	response = RowData.objects(Q(timestamp__gte=most_recent) & Q(participant_first_name=participant[0]) & Q(participant_last_name=participant_last_array)).to_json()
		# print(participant_last_array)
		# return response


	if request_type == "group":
		group = request.args.get('group', '')
		activity = request.args.get('activity', '')
		if group == "Design":
			response = RowData.objects(Q(timestamp__gte=most_recent) & Q(org_unit=group) & Q(activity_description="Design Prototype Due")).to_json()
			return response
		elif group == "Development":
			response = RowData.objects(Q(timestamp__gte=most_recent) & Q(org_unit="FED BGL") & Q(activity_description="Complete F.E.D.") | Q(timestamp__gte=most_recent) & Q(org_unit="FED EGN") & Q(activity_description="Complete F.E.D.")).to_json()
			return response


	# return return_data


@app.route('/update_data')
def updateData():
	try:
		output_data()
		return "Updated data"
	except Exception:
		return "Whoops! Data was not updated!"
    # sched = Scheduler()
    # sched.start()

    # job = sched.add_interval_job(output_data, minutes=2)
    # print("running main")
    # sched.print_jobs()
    # return ("running main")


@app.route('/login')
def login():
	app.logger.info(request.headers['host'])
	return redirect(auth.safe_url + '&siteLink=http://' + request.headers['host'] + '/')


@app.route('/logout')
def logout():
    session.clear()
    return redirect('/')

@app.route('/fake_ken')
def fake_ken():
    return render_template("fakeform_ken.html")

@app.route('/forbidden')
def forbidden():
    return render_template("forbidden.html")

@app.route('/get_user')
def get_user():
	if session["user"]:
		safe_id = session["user"]["safeID"]
		response = User.objects(safe_id=safe_id).first().to_json()
		return response
	else:
		data = {'status' : 'Error', 'message':'not logged in'}
		return json.dumps(data)

@app.route('/get_user_data')
def get_user_data():
	if session["user"]:
		safe_id = session["user"]["safeID"]
		print(safe_id)
		try:
			response = User_data.objects.get(user_id = safe_id ).to_json()
			return response
		except Exception:
			print("running exception")
			this_user = User.objects(safe_id=safe_id).first()
			new_user = User_data(safe_id, this_user)
			new_user.save()

			response = User_data.objects(user_id=safe_id).to_json()
			return response
	else:
		data = {'status' : 'Error', 'message':'not logged in'}
		return json.dumps(data)
	

@app.route('/update_user_data', methods = ['GET', 'POST'])
def update_user_data():
	request_data = request.get_json(force=True, cache=False)
	# pprint.pprint(request_data)
	# print (request_data)
	uid = session["user"]["safeID"]
	User_data.objects(user_id=uid).update_one(
		upsert=True,
		set__pref_view = request_data["pref_view"],
		set__pref_act_filter  = request_data["pref_activity_filter"],
		set__pref_manager_filter  = request_data["pref_manager_filter"],
		set__pref_product_filter = request_data["pref_product_filter"],
		set__pref_process_filter = request_data["pref_process_filter"]
	)
	updated_user = User_data.objects.get(user_id=uid).to_json()
	return updated_user

@app.route('/reset_user_prefs', methods = ['GET', 'POST'])
def reset_user_prefs():
	request_data = request.get_json(force=True, cache=False)
	client_id = request_data["user_id"]
	uid = session["user"]["safeID"]
	if client_id != uid:
		return "you aren't you"

	User_data.objects(user_id=uid).update_one(
		unset__pref_view = 1,
		unset__pref_act_filter = 1,
		unset__pref_manager_filter = 1,
		unset__pref_product_filter = 1,
		unset__pref_process_filter = 1
	)
	updated_user = User_data.objects.get(user_id=uid).to_json()
	return updated_user

@app.route('/write_note', methods = ['GET', 'POST'])
def write_note():
	request_data = request.get_json(force=True, cache=False)
	new_note = Note()
	new_note.row_data =json.dumps(request_data['rowData'])
	new_note.sub_id = request_data['subID']
	new_note.activity = request_data['activity']
	new_note.content = request_data['content']
	new_note.author = request_data['author']
	new_note.safeID = request_data['safeID']
	new_note.date = request_data['date']
	
	new_note.save()
	return new_note.to_json()

@app.route('/update_note', methods = ['GET', 'POST'])
def update_note():
	request_data = request.get_json(force=True, cache=False)
	note_id = request_data['id']
	
	note_user_id = request_data['safeID']
	note_content = request_data['content']
	if note_user_id != session["user"]["safeID"]:
		return "{error:'unauthorized', msg: 'not note author'}"
	updatedNote = Note.objects.get(note_id= note_id)
	updatedNote.update(set__content=note_content)
	
	return updatedNote.to_json()
	


@app.route('/get_notes', methods=['GET'])
def get_notes():
	response = Note.objects().to_json()
	return response


@app.route('/delete_note', methods=['GET', 'POST'])
def delete_note():
	request_data = request.get_json(force=True, cache=False)
	note_user_id = request_data['safeID']
	note_id = request_data['note_id']
	if note_user_id != session["user"]["safeID"]:
		return "{error:'unauthorized', msg: 'not note author -- may not delete'}"
	Note.objects(note_id = note_id).delete()

	
	return "note deleted"

@app.route('/safeauth', methods=['POST'])
def safeauth():
	uid = request.form.get('uid', '')
	email = request.form.get('email', '')
	firstname = request.form.get('firstname', '')
	lastname = request.form.get('lastname', '')

	

	# Make sure SAFE authentication is valid
	# valid, message = auth.validate(
	# uid=uid,
	# time_string=request.form.get('time', ''),
	# email=email,
	# firstname=firstname,
	# lastname=lastname,
	# digest=request.form.get('digest', ''),
	# )

	# if not valid:
	# 	return redirect('/forbidden')

	# Make sure user exists (or create one if not)

	User.objects(safe_id=uid).update_one(
	   set__safe_id=uid,
	   set__email=email,
	   set__first_name=firstname,
	   set__last_name=lastname,
	   upsert=True
	)
	user = User.objects(safe_id=uid).first()


	session['user'] = {
	   'safeID': user.safe_id,
	   'email': user.email,
	   'firstName': user.first_name,
	   'lastName': user.last_name,
	   'hideFeedbackMessage': user.hide_feedback_message,
	   'hideBrowserMessage': user.hide_browser_message,
	   
	}

	return redirect('/')	
