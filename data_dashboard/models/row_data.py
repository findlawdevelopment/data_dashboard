import datetime
from flask import url_for
from data_dashboard import db
from mongoengine import *

class RowData(db.Document):
    timestamp = db.DateTimeField(required=True)
    project_age = db.IntField(default="", required=True)
    participant_last_name = db.StringField(required=True)
    participant_first_name = db.StringField(required=True)
    org_unit = db.StringField(required=True)
    pm_name = db.StringField(required=True)
    firm_name = db.StringField(required=True)
    wld_id = db.IntField(default="", required=True)
    sub_id = db.IntField(default="", required=True)
    logical_site = db.IntField(default="", required=True)
    sap_type = db.StringField(required=True)
    account_value = db.IntField(default="", required=True)
    product = db.StringField(required=True)
    process_description = db.StringField(required=True)
    activity_description = db.StringField(required=True)
    activity_age = db.IntField(default="", required=True)
    activity_due = db.DateTimeField(required=True)
    completed_date = db.StringField(required=True)
    status = db.StringField(required=True)
    bpm_id = db.IntField(default="", required=True)
    cdc = db.StringField(required=True)
    rsm = db.StringField(required=True)
    writer = db.StringField(required=True)
    designer = db.StringField(required=True)
    seo = db.StringField(required=True)
    line_item_status = db.StringField(required=True)
    activity_status = db.StringField(required=True)
    project_status = db.StringField(required=True)
    priority = db.StringField(required=True)
    staff_manager = db.StringField(required=True)
    date_modified = db.DateTimeField(required=True)
    meta = {
        'indexes': [
                {'fields': ['timestamp'], 'expireAfterSeconds': 1209600}
            ]
    }

class Activity(db.EmbeddedDocument):
    label = db.StringField(required=True)
    green_project = db.IntField(default=None)
    yellow_project = db.IntField(default=None)


class Product(db.EmbeddedDocument):
    label = db.StringField(required=True)
    nodes = db.ListField(db.EmbeddedDocumentField(Activity))


class Thresholds(db.Document):
    # activity = db.StringField(required=True)
    # green_project = db.IntField(required=True)
    # yellow_project = db.IntField(required=True)
    label = db.StringField(required=True)
    nodes = db.ListField(db.EmbeddedDocumentField(Product))



