var app = angular.module('traffikDirectives', []);

//watch container height for changes
app.directive( 'emHeightTarget', function(){
    return {
        link: function( scope, elem, attrs ){
            scope.showMore = false;
/*            scope.elementHeight = elem[0].offsetHeight;
*/            scope.$watch( function(){
                    scope.elementHeight = elem[0].offsetHeight;
                    return scope.elementHeight;
                }, function( newHeight, oldHeight ){
/*                elem.attr( 'style', 'margin-top: ' + (58+newHeight) + 'px' );
*/                if (newHeight > 35){
                        scope.showMore = true;
                    }
                    else{
                        scope.showMore = false;
                    }

            } );
        }
    };
} );

