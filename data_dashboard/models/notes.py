import datetime
from flask import url_for
from mongoengine import *
from data_dashboard import db

class Note(db.Document):
	row_data = StringField(required = True)
	sub_id = IntField(default="", required=True)
	activity = StringField(required = True)
	content = StringField(default="", required=True)
	author = StringField(required = True)
	safeID = StringField(required = True)
	date = db.DateTimeField(required = True)
	note_id = SequenceField(required = True, primary_key = True)
