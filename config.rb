# Require any additional compass plugins here.
require "singularitygs"
# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "data_dashboard/static/css"
sass_dir = "sass"
images_dir = "data_dashboard/static/images"
javascripts_dir = "data_dashboard/static/js"

# You can select your preferred output style here (can be overridden via the command line):
output_style = :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false

preferred_syntax = :sass
