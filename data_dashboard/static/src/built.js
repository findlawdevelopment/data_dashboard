angular.module('ui.bootstrap.affix', [])
  .directive('affix', ['$window', '$document', '$parse',
    function($window, $document, $parse) {
      return {
        scope: {
          affix: '@'
        },
        link: function(scope, element, attrs) {
          var win = angular.element($window),
            affixed;

          // Obviously, whenever a scroll occurs, we need to check and possibly 
          // adjust the position of the affixed element.
          win.bind('scroll', checkPosition);

          // Less obviously, when a link is clicked (in theory changing the current
          // scroll position), we need to check and possibly adjsut the position. We,
          // however, can't do this instantly as the page may not be in the right
          // position yet.
          win.bind('click', function() {
            setTimeout(checkPosition, 1);
          });

          function checkPosition() {
            var offset = $parse(scope.affix)(scope);
            var affix = win.prop('pageYOffset') <= offset ? 'top' : false;

            if (affixed === affix) return;

            affixed = affix;

            element.removeClass('affix affix-top').addClass('affix' + (affix ? '-' + affix : ''));
          }
        }
      };
    }
  ]);;
/*
 * angular-ui-bootstrap
 * http://angular-ui.github.io/bootstrap/
 
 * Version: 0.10.0 - 2014-01-13
 * License: MIT
 */
angular.module("ui.bootstrap", ["ui.bootstrap.tpls", "ui.bootstrap.transition", "ui.bootstrap.collapse", "ui.bootstrap.accordion", "ui.bootstrap.alert", "ui.bootstrap.bindHtml", "ui.bootstrap.buttons", "ui.bootstrap.carousel", "ui.bootstrap.position", "ui.bootstrap.datepicker", "ui.bootstrap.dropdownToggle", "ui.bootstrap.modal", "ui.bootstrap.pagination", "ui.bootstrap.tooltip", "ui.bootstrap.popover", "ui.bootstrap.progressbar", "ui.bootstrap.rating", "ui.bootstrap.tabs", "ui.bootstrap.timepicker", "ui.bootstrap.typeahead"]), angular.module("ui.bootstrap.tpls", ["template/accordion/accordion-group.html", "template/accordion/accordion.html", "template/alert/alert.html", "template/carousel/carousel.html", "template/carousel/slide.html", "template/datepicker/datepicker.html", "template/datepicker/popup.html", "template/modal/backdrop.html", "template/modal/window.html", "template/pagination/pager.html", "template/pagination/pagination.html", "template/tooltip/tooltip-html-unsafe-popup.html", "template/tooltip/tooltip-popup.html", "template/popover/popover.html", "template/progressbar/bar.html", "template/progressbar/progress.html", "template/progressbar/progressbar.html", "template/rating/rating.html", "template/tabs/tab.html", "template/tabs/tabset.html", "template/timepicker/timepicker.html", "template/typeahead/typeahead-match.html", "template/typeahead/typeahead-popup.html"]), angular.module("ui.bootstrap.transition", []).factory("$transition", ["$q", "$timeout", "$rootScope",
  function(a, b, c) {
    function d(a) {
      for (var b in a)
        if (void 0 !== f.style[b]) return a[b]
    }
    var e = function(d, f, g) {
      g = g || {};
      var h = a.defer(),
        i = e[g.animation ? "animationEndEventName" : "transitionEndEventName"],
        j = function() {
          c.$apply(function() {
            d.unbind(i, j), h.resolve(d)
          })
        };
      return i && d.bind(i, j), b(function() {
        angular.isString(f) ? d.addClass(f) : angular.isFunction(f) ? f(d) : angular.isObject(f) && d.css(f), i || h.resolve(d)
      }), h.promise.cancel = function() {
        i && d.unbind(i, j), h.reject("Transition cancelled")
      }, h.promise
    }, f = document.createElement("trans"),
      g = {
        WebkitTransition: "webkitTransitionEnd",
        MozTransition: "transitionend",
        OTransition: "oTransitionEnd",
        transition: "transitionend"
      }, h = {
        WebkitTransition: "webkitAnimationEnd",
        MozTransition: "animationend",
        OTransition: "oAnimationEnd",
        transition: "animationend"
      };
    return e.transitionEndEventName = d(g), e.animationEndEventName = d(h), e
  }
]), angular.module("ui.bootstrap.collapse", ["ui.bootstrap.transition"]).directive("collapse", ["$transition",
  function(a) {
    return {
      link: function(b, c, d) {
        function e(b) {
          function d() {
            j === e && (j = void 0)
          }
          var e = a(c, b);
          return j && j.cancel(), j = e, e.then(d, d), e
        }

        function f() {
          k ? (k = !1, g()) : (c.removeClass("collapse").addClass("collapsing"), e({
            height: c[0].scrollHeight + "px"
          }).then(g))
        }

        function g() {
          c.removeClass("collapsing"), c.addClass("collapse in"), c.css({
            height: "auto"
          })
        }

        function h() {
          if (k) k = !1, i(), c.css({
            height: 0
          });
          else {
            c.css({
              height: c[0].scrollHeight + "px"
            }); {
              c[0].offsetWidth
            }
            c.removeClass("collapse in").addClass("collapsing"), e({
              height: 0
            }).then(i)
          }
        }

        function i() {
          c.removeClass("collapsing"), c.addClass("collapse")
        }
        var j, k = !0;
        b.$watch(d.collapse, function(a) {
          a ? h() : f()
        })
      }
    }
  }
]), angular.module("ui.bootstrap.accordion", ["ui.bootstrap.collapse"]).constant("accordionConfig", {
  closeOthers: !0
}).controller("AccordionController", ["$scope", "$attrs", "accordionConfig",
  function(a, b, c) {
    this.groups = [], this.closeOthers = function(d) {
      var e = angular.isDefined(b.closeOthers) ? a.$eval(b.closeOthers) : c.closeOthers;
      e && angular.forEach(this.groups, function(a) {
        a !== d && (a.isOpen = !1)
      })
    }, this.addGroup = function(a) {
      var b = this;
      this.groups.push(a), a.$on("$destroy", function() {
        b.removeGroup(a)
      })
    }, this.removeGroup = function(a) {
      var b = this.groups.indexOf(a); - 1 !== b && this.groups.splice(this.groups.indexOf(a), 1)
    }
  }
]).directive("accordion", function() {
  return {
    restrict: "EA",
    controller: "AccordionController",
    transclude: !0,
    replace: !1,
    templateUrl: "template/accordion/accordion.html"
  }
}).directive("accordionGroup", ["$parse",
  function(a) {
    return {
      require: "^accordion",
      restrict: "EA",
      transclude: !0,
      replace: !0,
      templateUrl: "template/accordion/accordion-group.html",
      scope: {
        heading: "@"
      },
      controller: function() {
        this.setHeading = function(a) {
          this.heading = a
        }
      },
      link: function(b, c, d, e) {
        var f, g;
        e.addGroup(b), b.isOpen = !1, d.isOpen && (f = a(d.isOpen), g = f.assign, b.$parent.$watch(f, function(a) {
          b.isOpen = !! a
        })), b.$watch("isOpen", function(a) {
          a && e.closeOthers(b), g && g(b.$parent, a)
        })
      }
    }
  }
]).directive("accordionHeading", function() {
  return {
    restrict: "EA",
    transclude: !0,
    template: "",
    replace: !0,
    require: "^accordionGroup",
    compile: function(a, b, c) {
      return function(a, b, d, e) {
        e.setHeading(c(a, function() {}))
      }
    }
  }
}).directive("accordionTransclude", function() {
  return {
    require: "^accordionGroup",
    link: function(a, b, c, d) {
      a.$watch(function() {
        return d[c.accordionTransclude]
      }, function(a) {
        a && (b.html(""), b.append(a))
      })
    }
  }
}), angular.module("ui.bootstrap.alert", []).controller("AlertController", ["$scope", "$attrs",
  function(a, b) {
    a.closeable = "close" in b
  }
]).directive("alert", function() {
  return {
    restrict: "EA",
    controller: "AlertController",
    templateUrl: "template/alert/alert.html",
    transclude: !0,
    replace: !0,
    scope: {
      type: "=",
      close: "&"
    }
  }
}), angular.module("ui.bootstrap.bindHtml", []).directive("bindHtmlUnsafe", function() {
  return function(a, b, c) {
    b.addClass("ng-binding").data("$binding", c.bindHtmlUnsafe), a.$watch(c.bindHtmlUnsafe, function(a) {
      b.html(a || "")
    })
  }
}), angular.module("ui.bootstrap.buttons", []).constant("buttonConfig", {
  activeClass: "active",
  toggleEvent: "click"
}).controller("ButtonsController", ["buttonConfig",
  function(a) {
    this.activeClass = a.activeClass || "active", this.toggleEvent = a.toggleEvent || "click"
  }
]).directive("btnRadio", function() {
  return {
    require: ["btnRadio", "ngModel"],
    controller: "ButtonsController",
    link: function(a, b, c, d) {
      var e = d[0],
        f = d[1];
      f.$render = function() {
        b.toggleClass(e.activeClass, angular.equals(f.$modelValue, a.$eval(c.btnRadio)))
      }, b.bind(e.toggleEvent, function() {
        b.hasClass(e.activeClass) || a.$apply(function() {
          f.$setViewValue(a.$eval(c.btnRadio)), f.$render()
        })
      })
    }
  }
}).directive("btnCheckbox", function() {
  return {
    require: ["btnCheckbox", "ngModel"],
    controller: "ButtonsController",
    link: function(a, b, c, d) {
      function e() {
        return g(c.btnCheckboxTrue, !0)
      }

      function f() {
        return g(c.btnCheckboxFalse, !1)
      }

      function g(b, c) {
        var d = a.$eval(b);
        return angular.isDefined(d) ? d : c
      }
      var h = d[0],
        i = d[1];
      i.$render = function() {
        b.toggleClass(h.activeClass, angular.equals(i.$modelValue, e()))
      }, b.bind(h.toggleEvent, function() {
        a.$apply(function() {
          i.$setViewValue(b.hasClass(h.activeClass) ? f() : e()), i.$render()
        })
      })
    }
  }
}), angular.module("ui.bootstrap.carousel", ["ui.bootstrap.transition"]).controller("CarouselController", ["$scope", "$timeout", "$transition", "$q",
  function(a, b, c) {
    function d() {
      e();
      var c = +a.interval;
      !isNaN(c) && c >= 0 && (g = b(f, c))
    }

    function e() {
      g && (b.cancel(g), g = null)
    }

    function f() {
      h ? (a.next(), d()) : a.pause()
    }
    var g, h, i = this,
      j = i.slides = [],
      k = -1;
    i.currentSlide = null;
    var l = !1;
    i.select = function(e, f) {
      function g() {
        if (!l) {
          if (i.currentSlide && angular.isString(f) && !a.noTransition && e.$element) {
            e.$element.addClass(f); {
              e.$element[0].offsetWidth
            }
            angular.forEach(j, function(a) {
              angular.extend(a, {
                direction: "",
                entering: !1,
                leaving: !1,
                active: !1
              })
            }), angular.extend(e, {
              direction: f,
              active: !0,
              entering: !0
            }), angular.extend(i.currentSlide || {}, {
              direction: f,
              leaving: !0
            }), a.$currentTransition = c(e.$element, {}),
            function(b, c) {
              a.$currentTransition.then(function() {
                h(b, c)
              }, function() {
                h(b, c)
              })
            }(e, i.currentSlide)
          } else h(e, i.currentSlide);
          i.currentSlide = e, k = m, d()
        }
      }

      function h(b, c) {
        angular.extend(b, {
          direction: "",
          active: !0,
          leaving: !1,
          entering: !1
        }), angular.extend(c || {}, {
          direction: "",
          active: !1,
          leaving: !1,
          entering: !1
        }), a.$currentTransition = null
      }
      var m = j.indexOf(e);
      void 0 === f && (f = m > k ? "next" : "prev"), e && e !== i.currentSlide && (a.$currentTransition ? (a.$currentTransition.cancel(), b(g)) : g())
    }, a.$on("$destroy", function() {
      l = !0
    }), i.indexOfSlide = function(a) {
      return j.indexOf(a)
    }, a.next = function() {
      var b = (k + 1) % j.length;
      return a.$currentTransition ? void 0 : i.select(j[b], "next")
    }, a.prev = function() {
      var b = 0 > k - 1 ? j.length - 1 : k - 1;
      return a.$currentTransition ? void 0 : i.select(j[b], "prev")
    }, a.select = function(a) {
      i.select(a)
    }, a.isActive = function(a) {
      return i.currentSlide === a
    }, a.slides = function() {
      return j
    }, a.$watch("interval", d), a.$on("$destroy", e), a.play = function() {
      h || (h = !0, d())
    }, a.pause = function() {
      a.noPause || (h = !1, e())
    }, i.addSlide = function(b, c) {
      b.$element = c, j.push(b), 1 === j.length || b.active ? (i.select(j[j.length - 1]), 1 == j.length && a.play()) : b.active = !1
    }, i.removeSlide = function(a) {
      var b = j.indexOf(a);
      j.splice(b, 1), j.length > 0 && a.active ? b >= j.length ? i.select(j[b - 1]) : i.select(j[b]) : k > b && k--
    }
  }
]).directive("carousel", [
  function() {
    return {
      restrict: "EA",
      transclude: !0,
      replace: !0,
      controller: "CarouselController",
      require: "carousel",
      templateUrl: "template/carousel/carousel.html",
      scope: {
        interval: "=",
        noTransition: "=",
        noPause: "="
      }
    }
  }
]).directive("slide", ["$parse",
  function(a) {
    return {
      require: "^carousel",
      restrict: "EA",
      transclude: !0,
      replace: !0,
      templateUrl: "template/carousel/slide.html",
      scope: {},
      link: function(b, c, d, e) {
        if (d.active) {
          var f = a(d.active),
            g = f.assign,
            h = b.active = f(b.$parent);
          b.$watch(function() {
            var a = f(b.$parent);
            return a !== b.active && (a !== h ? h = b.active = a : g(b.$parent, a = h = b.active)), a
          })
        }
        e.addSlide(b, c), b.$on("$destroy", function() {
          e.removeSlide(b)
        }), b.$watch("active", function(a) {
          a && e.select(b)
        })
      }
    }
  }
]), angular.module("ui.bootstrap.position", []).factory("$position", ["$document", "$window",
  function(a, b) {
    function c(a, c) {
      return a.currentStyle ? a.currentStyle[c] : b.getComputedStyle ? b.getComputedStyle(a)[c] : a.style[c]
    }

    function d(a) {
      return "static" === (c(a, "position") || "static")
    }
    var e = function(b) {
      for (var c = a[0], e = b.offsetParent || c; e && e !== c && d(e);) e = e.offsetParent;
      return e || c
    };
    return {
      position: function(b) {
        var c = this.offset(b),
          d = {
            top: 0,
            left: 0
          }, f = e(b[0]);
        f != a[0] && (d = this.offset(angular.element(f)), d.top += f.clientTop - f.scrollTop, d.left += f.clientLeft - f.scrollLeft);
        var g = b[0].getBoundingClientRect();
        return {
          width: g.width || b.prop("offsetWidth"),
          height: g.height || b.prop("offsetHeight"),
          top: c.top - d.top,
          left: c.left - d.left
        }
      },
      offset: function(c) {
        var d = c[0].getBoundingClientRect();
        return {
          width: d.width || c.prop("offsetWidth"),
          height: d.height || c.prop("offsetHeight"),
          top: d.top + (b.pageYOffset || a[0].body.scrollTop || a[0].documentElement.scrollTop),
          left: d.left + (b.pageXOffset || a[0].body.scrollLeft || a[0].documentElement.scrollLeft)
        }
      }
    }
  }
]), angular.module("ui.bootstrap.datepicker", ["ui.bootstrap.position"]).constant("datepickerConfig", {
  dayFormat: "dd",
  monthFormat: "MMMM",
  yearFormat: "yyyy",
  dayHeaderFormat: "EEE",
  dayTitleFormat: "MMMM yyyy",
  monthTitleFormat: "yyyy",
  showWeeks: !0,
  startingDay: 0,
  yearRange: 20,
  minDate: null,
  maxDate: null
}).controller("DatepickerController", ["$scope", "$attrs", "dateFilter", "datepickerConfig",
  function(a, b, c, d) {
    function e(b, c) {
      return angular.isDefined(b) ? a.$parent.$eval(b) : c
    }

    function f(a, b) {
      return new Date(a, b, 0).getDate()
    }

    function g(a, b) {
      for (var c = new Array(b), d = a, e = 0; b > e;) c[e++] = new Date(d), d.setDate(d.getDate() + 1);
      return c
    }

    function h(a, b, d, e) {
      return {
        date: a,
        label: c(a, b),
        selected: !! d,
        secondary: !! e
      }
    }
    var i = {
      day: e(b.dayFormat, d.dayFormat),
      month: e(b.monthFormat, d.monthFormat),
      year: e(b.yearFormat, d.yearFormat),
      dayHeader: e(b.dayHeaderFormat, d.dayHeaderFormat),
      dayTitle: e(b.dayTitleFormat, d.dayTitleFormat),
      monthTitle: e(b.monthTitleFormat, d.monthTitleFormat)
    }, j = e(b.startingDay, d.startingDay),
      k = e(b.yearRange, d.yearRange);
    this.minDate = d.minDate ? new Date(d.minDate) : null, this.maxDate = d.maxDate ? new Date(d.maxDate) : null, this.modes = [{
      name: "day",
      getVisibleDates: function(a, b) {
        var d = a.getFullYear(),
          e = a.getMonth(),
          k = new Date(d, e, 1),
          l = j - k.getDay(),
          m = l > 0 ? 7 - l : -l,
          n = new Date(k),
          o = 0;
        m > 0 && (n.setDate(-m + 1), o += m), o += f(d, e + 1), o += (7 - o % 7) % 7;
        for (var p = g(n, o), q = new Array(7), r = 0; o > r; r++) {
          var s = new Date(p[r]);
          p[r] = h(s, i.day, b && b.getDate() === s.getDate() && b.getMonth() === s.getMonth() && b.getFullYear() === s.getFullYear(), s.getMonth() !== e)
        }
        for (var t = 0; 7 > t; t++) q[t] = c(p[t].date, i.dayHeader);
        return {
          objects: p,
          title: c(a, i.dayTitle),
          labels: q
        }
      },
      compare: function(a, b) {
        return new Date(a.getFullYear(), a.getMonth(), a.getDate()) - new Date(b.getFullYear(), b.getMonth(), b.getDate())
      },
      split: 7,
      step: {
        months: 1
      }
    }, {
      name: "month",
      getVisibleDates: function(a, b) {
        for (var d = new Array(12), e = a.getFullYear(), f = 0; 12 > f; f++) {
          var g = new Date(e, f, 1);
          d[f] = h(g, i.month, b && b.getMonth() === f && b.getFullYear() === e)
        }
        return {
          objects: d,
          title: c(a, i.monthTitle)
        }
      },
      compare: function(a, b) {
        return new Date(a.getFullYear(), a.getMonth()) - new Date(b.getFullYear(), b.getMonth())
      },
      split: 3,
      step: {
        years: 1
      }
    }, {
      name: "year",
      getVisibleDates: function(a, b) {
        for (var c = new Array(k), d = a.getFullYear(), e = parseInt((d - 1) / k, 10) * k + 1, f = 0; k > f; f++) {
          var g = new Date(e + f, 0, 1);
          c[f] = h(g, i.year, b && b.getFullYear() === g.getFullYear())
        }
        return {
          objects: c,
          title: [c[0].label, c[k - 1].label].join(" - ")
        }
      },
      compare: function(a, b) {
        return a.getFullYear() - b.getFullYear()
      },
      split: 5,
      step: {
        years: k
      }
    }], this.isDisabled = function(b, c) {
      var d = this.modes[c || 0];
      return this.minDate && d.compare(b, this.minDate) < 0 || this.maxDate && d.compare(b, this.maxDate) > 0 || a.dateDisabled && a.dateDisabled({
        date: b,
        mode: d.name
      })
    }
  }
]).directive("datepicker", ["dateFilter", "$parse", "datepickerConfig", "$log",
  function(a, b, c, d) {
    return {
      restrict: "EA",
      replace: !0,
      templateUrl: "template/datepicker/datepicker.html",
      scope: {
        dateDisabled: "&"
      },
      require: ["datepicker", "?^ngModel"],
      controller: "DatepickerController",
      link: function(a, e, f, g) {
        function h() {
          a.showWeekNumbers = 0 === o && q
        }

        function i(a, b) {
          for (var c = []; a.length > 0;) c.push(a.splice(0, b));
          return c
        }

        function j(b) {
          var c = null,
            e = !0;
          n.$modelValue && (c = new Date(n.$modelValue), isNaN(c) ? (e = !1, d.error('Datepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.')) : b && (p = c)), n.$setValidity("date", e);
          var f = m.modes[o],
            g = f.getVisibleDates(p, c);
          angular.forEach(g.objects, function(a) {
            a.disabled = m.isDisabled(a.date, o)
          }), n.$setValidity("date-disabled", !c || !m.isDisabled(c)), a.rows = i(g.objects, f.split), a.labels = g.labels || [], a.title = g.title
        }

        function k(a) {
          o = a, h(), j()
        }

        function l(a) {
          var b = new Date(a);
          b.setDate(b.getDate() + 4 - (b.getDay() || 7));
          var c = b.getTime();
          return b.setMonth(0), b.setDate(1), Math.floor(Math.round((c - b) / 864e5) / 7) + 1
        }
        var m = g[0],
          n = g[1];
        if (n) {
          var o = 0,
            p = new Date,
            q = c.showWeeks;
          f.showWeeks ? a.$parent.$watch(b(f.showWeeks), function(a) {
            q = !! a, h()
          }) : h(), f.min && a.$parent.$watch(b(f.min), function(a) {
            m.minDate = a ? new Date(a) : null, j()
          }), f.max && a.$parent.$watch(b(f.max), function(a) {
            m.maxDate = a ? new Date(a) : null, j()
          }), n.$render = function() {
            j(!0)
          }, a.select = function(a) {
            if (0 === o) {
              var b = n.$modelValue ? new Date(n.$modelValue) : new Date(0, 0, 0, 0, 0, 0, 0);
              b.setFullYear(a.getFullYear(), a.getMonth(), a.getDate()), n.$setViewValue(b), j(!0)
            } else p = a, k(o - 1)
          }, a.move = function(a) {
            var b = m.modes[o].step;
            p.setMonth(p.getMonth() + a * (b.months || 0)), p.setFullYear(p.getFullYear() + a * (b.years || 0)), j()
          }, a.toggleMode = function() {
            k((o + 1) % m.modes.length)
          }, a.getWeekNumber = function(b) {
            return 0 === o && a.showWeekNumbers && 7 === b.length ? l(b[0].date) : null
          }
        }
      }
    }
  }
]).constant("datepickerPopupConfig", {
  dateFormat: "yyyy-MM-dd",
  currentText: "Today",
  toggleWeeksText: "Weeks",
  clearText: "Clear",
  closeText: "Done",
  closeOnDateSelection: !0,
  appendToBody: !1,
  showButtonBar: !0
}).directive("datepickerPopup", ["$compile", "$parse", "$document", "$position", "dateFilter", "datepickerPopupConfig", "datepickerConfig",
  function(a, b, c, d, e, f, g) {
    return {
      restrict: "EA",
      require: "ngModel",
      link: function(h, i, j, k) {
        function l(a) {
          u ? u(h, !! a) : q.isOpen = !! a
        }

        function m(a) {
          if (a) {
            if (angular.isDate(a)) return k.$setValidity("date", !0), a;
            if (angular.isString(a)) {
              var b = new Date(a);
              return isNaN(b) ? (k.$setValidity("date", !1), void 0) : (k.$setValidity("date", !0), b)
            }
            return k.$setValidity("date", !1), void 0
          }
          return k.$setValidity("date", !0), null
        }

        function n(a, c, d) {
          a && (h.$watch(b(a), function(a) {
            q[c] = a
          }), y.attr(d || c, c))
        }

        function o() {
          q.position = s ? d.offset(i) : d.position(i), q.position.top = q.position.top + i.prop("offsetHeight")
        }
        var p, q = h.$new(),
          r = angular.isDefined(j.closeOnDateSelection) ? h.$eval(j.closeOnDateSelection) : f.closeOnDateSelection,
          s = angular.isDefined(j.datepickerAppendToBody) ? h.$eval(j.datepickerAppendToBody) : f.appendToBody;
        j.$observe("datepickerPopup", function(a) {
          p = a || f.dateFormat, k.$render()
        }), q.showButtonBar = angular.isDefined(j.showButtonBar) ? h.$eval(j.showButtonBar) : f.showButtonBar, h.$on("$destroy", function() {
          C.remove(), q.$destroy()
        }), j.$observe("currentText", function(a) {
          q.currentText = angular.isDefined(a) ? a : f.currentText
        }), j.$observe("toggleWeeksText", function(a) {
          q.toggleWeeksText = angular.isDefined(a) ? a : f.toggleWeeksText
        }), j.$observe("clearText", function(a) {
          q.clearText = angular.isDefined(a) ? a : f.clearText
        }), j.$observe("closeText", function(a) {
          q.closeText = angular.isDefined(a) ? a : f.closeText
        });
        var t, u;
        j.isOpen && (t = b(j.isOpen), u = t.assign, h.$watch(t, function(a) {
          q.isOpen = !! a
        })), q.isOpen = t ? t(h) : !1;
        var v = function(a) {
          q.isOpen && a.target !== i[0] && q.$apply(function() {
            l(!1)
          })
        }, w = function() {
            q.$apply(function() {
              l(!0)
            })
          }, x = angular.element("<div datepicker-popup-wrap><div datepicker></div></div>");
        x.attr({
          "ng-model": "date",
          "ng-change": "dateSelection()"
        });
        var y = angular.element(x.children()[0]),
          z = {};
        j.datepickerOptions && (z = h.$eval(j.datepickerOptions), y.attr(angular.extend({}, z))), k.$parsers.unshift(m), q.dateSelection = function(a) {
          angular.isDefined(a) && (q.date = a), k.$setViewValue(q.date), k.$render(), r && l(!1)
        }, i.bind("input change keyup", function() {
          q.$apply(function() {
            q.date = k.$modelValue
          })
        }), k.$render = function() {
          var a = k.$viewValue ? e(k.$viewValue, p) : "";
          i.val(a), q.date = k.$modelValue
        }, n(j.min, "min"), n(j.max, "max"), j.showWeeks ? n(j.showWeeks, "showWeeks", "show-weeks") : (q.showWeeks = "show-weeks" in z ? z["show-weeks"] : g.showWeeks, y.attr("show-weeks", "showWeeks")), j.dateDisabled && y.attr("date-disabled", j.dateDisabled);
        var A = !1,
          B = !1;
        q.$watch("isOpen", function(a) {
          a ? (o(), c.bind("click", v), B && i.unbind("focus", w), i[0].focus(), A = !0) : (A && c.unbind("click", v), i.bind("focus", w), B = !0), u && u(h, a)
        }), q.today = function() {
          q.dateSelection(new Date)
        }, q.clear = function() {
          q.dateSelection(null)
        };
        var C = a(x)(q);
        s ? c.find("body").append(C) : i.after(C)
      }
    }
  }
]).directive("datepickerPopupWrap", function() {
  return {
    restrict: "EA",
    replace: !0,
    transclude: !0,
    templateUrl: "template/datepicker/popup.html",
    link: function(a, b) {
      b.bind("click", function(a) {
        a.preventDefault(), a.stopPropagation()
      })
    }
  }
}), angular.module("ui.bootstrap.dropdownToggle", []).directive("dropdownToggle", ["$document", "$location",
  function(a) {
    var b = null,
      c = angular.noop;
    return {
      restrict: "CA",
      link: function(d, e) {
        d.$watch("$location.path", function() {
          c()
        }), e.parent().bind("click", function() {
          c()
        }), e.bind("click", function(d) {
          var f = e === b;
          d.preventDefault(), d.stopPropagation(), b && c(), f || e.hasClass("disabled") || e.prop("disabled") || (e.parent().addClass("open"), b = e, c = function(d) {
            d && (d.preventDefault(), d.stopPropagation()), a.unbind("click", c), e.parent().removeClass("open"), c = angular.noop, b = null
          }, a.bind("click", c))
        })
      }
    }
  }
]), angular.module("ui.bootstrap.modal", ["ui.bootstrap.transition"]).factory("$$stackedMap", function() {
  return {
    createNew: function() {
      var a = [];
      return {
        add: function(b, c) {
          a.push({
            key: b,
            value: c
          })
        },
        get: function(b) {
          for (var c = 0; c < a.length; c++)
            if (b == a[c].key) return a[c]
        },
        keys: function() {
          for (var b = [], c = 0; c < a.length; c++) b.push(a[c].key);
          return b
        },
        top: function() {
          return a[a.length - 1]
        },
        remove: function(b) {
          for (var c = -1, d = 0; d < a.length; d++)
            if (b == a[d].key) {
              c = d;
              break
            }
          return a.splice(c, 1)[0]
        },
        removeTop: function() {
          return a.splice(a.length - 1, 1)[0]
        },
        length: function() {
          return a.length
        }
      }
    }
  }
}).directive("modalBackdrop", ["$timeout",
  function(a) {
    return {
      restrict: "EA",
      replace: !0,
      templateUrl: "template/modal/backdrop.html",
      link: function(b) {
        b.animate = !1, a(function() {
          b.animate = !0
        })
      }
    }
  }
]).directive("modalWindow", ["$modalStack", "$timeout",
  function(a, b) {
    return {
      restrict: "EA",
      scope: {
        index: "@",
        animate: "="
      },
      replace: !0,
      transclude: !0,
      templateUrl: "template/modal/window.html",
      link: function(c, d, e) {
        c.windowClass = e.windowClass || "", b(function() {
          c.animate = !0, d[0].focus()
        }), c.close = function(b) {
          var c = a.getTop();
          c && c.value.backdrop && "static" != c.value.backdrop && b.target === b.currentTarget && (b.preventDefault(), b.stopPropagation(), a.dismiss(c.key, "backdrop click"))
        }
      }
    }
  }
]).factory("$modalStack", ["$transition", "$timeout", "$document", "$compile", "$rootScope", "$$stackedMap",
  function(a, b, c, d, e, f) {
    function g() {
      for (var a = -1, b = n.keys(), c = 0; c < b.length; c++) n.get(b[c]).value.backdrop && (a = c);
      return a
    }

    function h(a) {
      var b = c.find("body").eq(0),
        d = n.get(a).value;
      n.remove(a), j(d.modalDomEl, d.modalScope, 300, i), b.toggleClass(m, n.length() > 0)
    }

    function i() {
      if (k && -1 == g()) {
        var a = l;
        j(k, l, 150, function() {
          a.$destroy(), a = null
        }), k = void 0, l = void 0
      }
    }

    function j(c, d, e, f) {
      function g() {
        g.done || (g.done = !0, c.remove(), f && f())
      }
      d.animate = !1;
      var h = a.transitionEndEventName;
      if (h) {
        var i = b(g, e);
        c.bind(h, function() {
          b.cancel(i), g(), d.$apply()
        })
      } else b(g, 0)
    }
    var k, l, m = "modal-open",
      n = f.createNew(),
      o = {};
    return e.$watch(g, function(a) {
      l && (l.index = a)
    }), c.bind("keydown", function(a) {
      var b;
      27 === a.which && (b = n.top(), b && b.value.keyboard && e.$apply(function() {
        o.dismiss(b.key)
      }))
    }), o.open = function(a, b) {
      n.add(a, {
        deferred: b.deferred,
        modalScope: b.scope,
        backdrop: b.backdrop,
        keyboard: b.keyboard
      });
      var f = c.find("body").eq(0),
        h = g();
      h >= 0 && !k && (l = e.$new(!0), l.index = h, k = d("<div modal-backdrop></div>")(l), f.append(k));
      var i = angular.element("<div modal-window></div>");
      i.attr("window-class", b.windowClass), i.attr("index", n.length() - 1), i.attr("animate", "animate"), i.html(b.content);
      var j = d(i)(b.scope);
      n.top().value.modalDomEl = j, f.append(j), f.addClass(m)
    }, o.close = function(a, b) {
      var c = n.get(a).value;
      c && (c.deferred.resolve(b), h(a))
    }, o.dismiss = function(a, b) {
      var c = n.get(a).value;
      c && (c.deferred.reject(b), h(a))
    }, o.dismissAll = function(a) {
      for (var b = this.getTop(); b;) this.dismiss(b.key, a), b = this.getTop()
    }, o.getTop = function() {
      return n.top()
    }, o
  }
]).provider("$modal", function() {
  var a = {
    options: {
      backdrop: !0,
      keyboard: !0
    },
    $get: ["$injector", "$rootScope", "$q", "$http", "$templateCache", "$controller", "$modalStack",
      function(b, c, d, e, f, g, h) {
        function i(a) {
          return a.template ? d.when(a.template) : e.get(a.templateUrl, {
            cache: f
          }).then(function(a) {
            return a.data
          })
        }

        function j(a) {
          var c = [];
          return angular.forEach(a, function(a) {
            (angular.isFunction(a) || angular.isArray(a)) && c.push(d.when(b.invoke(a)))
          }), c
        }
        var k = {};
        return k.open = function(b) {
          var e = d.defer(),
            f = d.defer(),
            k = {
              result: e.promise,
              opened: f.promise,
              close: function(a) {
                h.close(k, a)
              },
              dismiss: function(a) {
                h.dismiss(k, a)
              }
            };
          if (b = angular.extend({}, a.options, b), b.resolve = b.resolve || {}, !b.template && !b.templateUrl) throw new Error("One of template or templateUrl options is required.");
          var l = d.all([i(b)].concat(j(b.resolve)));
          return l.then(function(a) {
            var d = (b.scope || c).$new();
            d.$close = k.close, d.$dismiss = k.dismiss;
            var f, i = {}, j = 1;
            b.controller && (i.$scope = d, i.$modalInstance = k, angular.forEach(b.resolve, function(b, c) {
              i[c] = a[j++]
            }), f = g(b.controller, i)), h.open(k, {
              scope: d,
              deferred: e,
              content: a[0],
              backdrop: b.backdrop,
              keyboard: b.keyboard,
              windowClass: b.windowClass
            })
          }, function(a) {
            e.reject(a)
          }), l.then(function() {
            f.resolve(!0)
          }, function() {
            f.reject(!1)
          }), k
        }, k
      }
    ]
  };
  return a
}), angular.module("ui.bootstrap.pagination", []).controller("PaginationController", ["$scope", "$attrs", "$parse", "$interpolate",
  function(a, b, c, d) {
    var e = this,
      f = b.numPages ? c(b.numPages).assign : angular.noop;
    this.init = function(d) {
      b.itemsPerPage ? a.$parent.$watch(c(b.itemsPerPage), function(b) {
        e.itemsPerPage = parseInt(b, 10), a.totalPages = e.calculateTotalPages()
      }) : this.itemsPerPage = d
    }, this.noPrevious = function() {
      return 1 === this.page
    }, this.noNext = function() {
      return this.page === a.totalPages
    }, this.isActive = function(a) {
      return this.page === a
    }, this.calculateTotalPages = function() {
      var b = this.itemsPerPage < 1 ? 1 : Math.ceil(a.totalItems / this.itemsPerPage);
      return Math.max(b || 0, 1)
    }, this.getAttributeValue = function(b, c, e) {
      return angular.isDefined(b) ? e ? d(b)(a.$parent) : a.$parent.$eval(b) : c
    }, this.render = function() {
      this.page = parseInt(a.page, 10) || 1, this.page > 0 && this.page <= a.totalPages && (a.pages = this.getPages(this.page, a.totalPages))
    }, a.selectPage = function(b) {
      !e.isActive(b) && b > 0 && b <= a.totalPages && (a.page = b, a.onSelectPage({
        page: b
      }))
    }, a.$watch("page", function() {
      e.render()
    }), a.$watch("totalItems", function() {
      a.totalPages = e.calculateTotalPages()
    }), a.$watch("totalPages", function(b) {
      f(a.$parent, b), e.page > b ? a.selectPage(b) : e.render()
    })
  }
]).constant("paginationConfig", {
  itemsPerPage: 10,
  boundaryLinks: !1,
  directionLinks: !0,
  firstText: "First",
  previousText: "Previous",
  nextText: "Next",
  lastText: "Last",
  rotate: !0
}).directive("pagination", ["$parse", "paginationConfig",
  function(a, b) {
    return {
      restrict: "EA",
      scope: {
        page: "=",
        totalItems: "=",
        onSelectPage: " &"
      },
      controller: "PaginationController",
      templateUrl: "template/pagination/pagination.html",
      replace: !0,
      link: function(c, d, e, f) {
        function g(a, b, c, d) {
          return {
            number: a,
            text: b,
            active: c,
            disabled: d
          }
        }
        var h, i = f.getAttributeValue(e.boundaryLinks, b.boundaryLinks),
          j = f.getAttributeValue(e.directionLinks, b.directionLinks),
          k = f.getAttributeValue(e.firstText, b.firstText, !0),
          l = f.getAttributeValue(e.previousText, b.previousText, !0),
          m = f.getAttributeValue(e.nextText, b.nextText, !0),
          n = f.getAttributeValue(e.lastText, b.lastText, !0),
          o = f.getAttributeValue(e.rotate, b.rotate);
        f.init(b.itemsPerPage), e.maxSize && c.$parent.$watch(a(e.maxSize), function(a) {
          h = parseInt(a, 10), f.render()
        }), f.getPages = function(a, b) {
          var c = [],
            d = 1,
            e = b,
            p = angular.isDefined(h) && b > h;
          p && (o ? (d = Math.max(a - Math.floor(h / 2), 1), e = d + h - 1, e > b && (e = b, d = e - h + 1)) : (d = (Math.ceil(a / h) - 1) * h + 1, e = Math.min(d + h - 1, b)));
          for (var q = d; e >= q; q++) {
            var r = g(q, q, f.isActive(q), !1);
            c.push(r)
          }
          if (p && !o) {
            if (d > 1) {
              var s = g(d - 1, "...", !1, !1);
              c.unshift(s)
            }
            if (b > e) {
              var t = g(e + 1, "...", !1, !1);
              c.push(t)
            }
          }
          if (j) {
            var u = g(a - 1, l, !1, f.noPrevious());
            c.unshift(u);
            var v = g(a + 1, m, !1, f.noNext());
            c.push(v)
          }
          if (i) {
            var w = g(1, k, !1, f.noPrevious());
            c.unshift(w);
            var x = g(b, n, !1, f.noNext());
            c.push(x)
          }
          return c
        }
      }
    }
  }
]).constant("pagerConfig", {
  itemsPerPage: 10,
  previousText: "« Previous",
  nextText: "Next »",
  align: !0
}).directive("pager", ["pagerConfig",
  function(a) {
    return {
      restrict: "EA",
      scope: {
        page: "=",
        totalItems: "=",
        onSelectPage: " &"
      },
      controller: "PaginationController",
      templateUrl: "template/pagination/pager.html",
      replace: !0,
      link: function(b, c, d, e) {
        function f(a, b, c, d, e) {
          return {
            number: a,
            text: b,
            disabled: c,
            previous: i && d,
            next: i && e
          }
        }
        var g = e.getAttributeValue(d.previousText, a.previousText, !0),
          h = e.getAttributeValue(d.nextText, a.nextText, !0),
          i = e.getAttributeValue(d.align, a.align);
        e.init(a.itemsPerPage), e.getPages = function(a) {
          return [f(a - 1, g, e.noPrevious(), !0, !1), f(a + 1, h, e.noNext(), !1, !0)]
        }
      }
    }
  }
]), angular.module("ui.bootstrap.tooltip", ["ui.bootstrap.position", "ui.bootstrap.bindHtml"]).provider("$tooltip", function() {
  function a(a) {
    var b = /[A-Z]/g,
      c = "-";
    return a.replace(b, function(a, b) {
      return (b ? c : "") + a.toLowerCase()
    })
  }
  var b = {
    placement: "top",
    animation: !0,
    popupDelay: 0
  }, c = {
      mouseenter: "mouseleave",
      click: "click",
      focus: "blur"
    }, d = {};
  this.options = function(a) {
    angular.extend(d, a)
  }, this.setTriggers = function(a) {
    angular.extend(c, a)
  }, this.$get = ["$window", "$compile", "$timeout", "$parse", "$document", "$position", "$interpolate",
    function(e, f, g, h, i, j, k) {
      return function(e, l, m) {
        function n(a) {
          var b = a || o.trigger || m,
            d = c[b] || b;
          return {
            show: b,
            hide: d
          }
        }
        var o = angular.extend({}, b, d),
          p = a(e),
          q = k.startSymbol(),
          r = k.endSymbol(),
          s = "<div " + p + '-popup title="' + q + "tt_title" + r + '" content="' + q + "tt_content" + r + '" placement="' + q + "tt_placement" + r + '" animation="tt_animation" is-open="tt_isOpen"></div>';
        return {
          restrict: "EA",
          scope: !0,
          compile: function() {
            var a = f(s);
            return function(b, c, d) {
              function f() {
                b.tt_isOpen ? m() : k()
              }

              function k() {
                (!z || b.$eval(d[l + "Enable"])) && (b.tt_popupDelay ? (v = g(p, b.tt_popupDelay, !1), v.then(function(a) {
                  a()
                })) : p()())
              }

              function m() {
                b.$apply(function() {
                  q()
                })
              }

              function p() {
                return b.tt_content ? (r(), u && g.cancel(u), t.css({
                  top: 0,
                  left: 0,
                  display: "block"
                }), w ? i.find("body").append(t) : c.after(t), A(), b.tt_isOpen = !0, b.$digest(), A) : angular.noop
              }

              function q() {
                b.tt_isOpen = !1, g.cancel(v), b.tt_animation ? u = g(s, 500) : s()
              }

              function r() {
                t && s(), t = a(b, function() {}), b.$digest()
              }

              function s() {
                t && (t.remove(), t = null)
              }
              var t, u, v, w = angular.isDefined(o.appendToBody) ? o.appendToBody : !1,
                x = n(void 0),
                y = !1,
                z = angular.isDefined(d[l + "Enable"]),
                A = function() {
                  var a, d, e, f;
                  switch (a = w ? j.offset(c) : j.position(c), d = t.prop("offsetWidth"), e = t.prop("offsetHeight"), b.tt_placement) {
                    case "right":
                      f = {
                        top: a.top + a.height / 2 - e / 2,
                        left: a.left + a.width
                      };
                      break;
                    case "bottom":
                      f = {
                        top: a.top + a.height,
                        left: a.left + a.width / 2 - d / 2
                      };
                      break;
                    case "left":
                      f = {
                        top: a.top + a.height / 2 - e / 2,
                        left: a.left - d
                      };
                      break;
                    default:
                      f = {
                        top: a.top - e,
                        left: a.left + a.width / 2 - d / 2
                      }
                  }
                  f.top += "px", f.left += "px", t.css(f)
                };
              b.tt_isOpen = !1, d.$observe(e, function(a) {
                b.tt_content = a, !a && b.tt_isOpen && q()
              }), d.$observe(l + "Title", function(a) {
                b.tt_title = a
              }), d.$observe(l + "Placement", function(a) {
                b.tt_placement = angular.isDefined(a) ? a : o.placement
              }), d.$observe(l + "PopupDelay", function(a) {
                var c = parseInt(a, 10);
                b.tt_popupDelay = isNaN(c) ? o.popupDelay : c
              });
              var B = function() {
                y && (c.unbind(x.show, k), c.unbind(x.hide, m))
              };
              d.$observe(l + "Trigger", function(a) {
                B(), x = n(a), x.show === x.hide ? c.bind(x.show, f) : (c.bind(x.show, k), c.bind(x.hide, m)), y = !0
              });
              var C = b.$eval(d[l + "Animation"]);
              b.tt_animation = angular.isDefined(C) ? !! C : o.animation, d.$observe(l + "AppendToBody", function(a) {
                w = angular.isDefined(a) ? h(a)(b) : w
              }), w && b.$on("$locationChangeSuccess", function() {
                b.tt_isOpen && q()
              }), b.$on("$destroy", function() {
                g.cancel(u), g.cancel(v), B(), s()
              })
            }
          }
        }
      }
    }
  ]
}).directive("tooltipPopup", function() {
  return {
    restrict: "EA",
    replace: !0,
    scope: {
      content: "@",
      placement: "@",
      animation: "&",
      isOpen: "&"
    },
    templateUrl: "template/tooltip/tooltip-popup.html"
  }
}).directive("tooltip", ["$tooltip",
  function(a) {
    return a("tooltip", "tooltip", "mouseenter")
  }
]).directive("tooltipHtmlUnsafePopup", function() {
  return {
    restrict: "EA",
    replace: !0,
    scope: {
      content: "@",
      placement: "@",
      animation: "&",
      isOpen: "&"
    },
    templateUrl: "template/tooltip/tooltip-html-unsafe-popup.html"
  }
}).directive("tooltipHtmlUnsafe", ["$tooltip",
  function(a) {
    return a("tooltipHtmlUnsafe", "tooltip", "mouseenter")
  }
]), angular.module("ui.bootstrap.popover", ["ui.bootstrap.tooltip"]).directive("popoverPopup", function() {
  return {
    restrict: "EA",
    replace: !0,
    scope: {
      title: "@",
      content: "@",
      placement: "@",
      animation: "&",
      isOpen: "&"
    },
    templateUrl: "template/popover/popover.html"
  }
}).directive("popover", ["$tooltip",
  function(a) {
    return a("popover", "popover", "click")
  }
]), angular.module("ui.bootstrap.progressbar", ["ui.bootstrap.transition"]).constant("progressConfig", {
  animate: !0,
  max: 100
}).controller("ProgressController", ["$scope", "$attrs", "progressConfig", "$transition",
  function(a, b, c, d) {
    var e = this,
      f = [],
      g = angular.isDefined(b.max) ? a.$parent.$eval(b.max) : c.max,
      h = angular.isDefined(b.animate) ? a.$parent.$eval(b.animate) : c.animate;
    this.addBar = function(a, b) {
      var c = 0,
        d = a.$parent.$index;
      angular.isDefined(d) && f[d] && (c = f[d].value), f.push(a), this.update(b, a.value, c), a.$watch("value", function(a, c) {
        a !== c && e.update(b, a, c)
      }), a.$on("$destroy", function() {
        e.removeBar(a)
      })
    }, this.update = function(a, b, c) {
      var e = this.getPercentage(b);
      h ? (a.css("width", this.getPercentage(c) + "%"), d(a, {
        width: e + "%"
      })) : a.css({
        transition: "none",
        width: e + "%"
      })
    }, this.removeBar = function(a) {
      f.splice(f.indexOf(a), 1)
    }, this.getPercentage = function(a) {
      return Math.round(100 * a / g)
    }
  }
]).directive("progress", function() {
  return {
    restrict: "EA",
    replace: !0,
    transclude: !0,
    controller: "ProgressController",
    require: "progress",
    scope: {},
    template: '<div class="progress" ng-transclude></div>'
  }
}).directive("bar", function() {
  return {
    restrict: "EA",
    replace: !0,
    transclude: !0,
    require: "^progress",
    scope: {
      value: "=",
      type: "@"
    },
    templateUrl: "template/progressbar/bar.html",
    link: function(a, b, c, d) {
      d.addBar(a, b)
    }
  }
}).directive("progressbar", function() {
  return {
    restrict: "EA",
    replace: !0,
    transclude: !0,
    controller: "ProgressController",
    scope: {
      value: "=",
      type: "@"
    },
    templateUrl: "template/progressbar/progressbar.html",
    link: function(a, b, c, d) {
      d.addBar(a, angular.element(b.children()[0]))
    }
  }
}), angular.module("ui.bootstrap.rating", []).constant("ratingConfig", {
  max: 5,
  stateOn: null,
  stateOff: null
}).controller("RatingController", ["$scope", "$attrs", "$parse", "ratingConfig",
  function(a, b, c, d) {
    this.maxRange = angular.isDefined(b.max) ? a.$parent.$eval(b.max) : d.max, this.stateOn = angular.isDefined(b.stateOn) ? a.$parent.$eval(b.stateOn) : d.stateOn, this.stateOff = angular.isDefined(b.stateOff) ? a.$parent.$eval(b.stateOff) : d.stateOff, this.createRateObjects = function(a) {
      for (var b = {
        stateOn: this.stateOn,
        stateOff: this.stateOff
      }, c = 0, d = a.length; d > c; c++) a[c] = angular.extend({
        index: c
      }, b, a[c]);
      return a
    }, a.range = angular.isDefined(b.ratingStates) ? this.createRateObjects(angular.copy(a.$parent.$eval(b.ratingStates))) : this.createRateObjects(new Array(this.maxRange)), a.rate = function(b) {
      a.value === b || a.readonly || (a.value = b)
    }, a.enter = function(b) {
      a.readonly || (a.val = b), a.onHover({
        value: b
      })
    }, a.reset = function() {
      a.val = angular.copy(a.value), a.onLeave()
    }, a.$watch("value", function(b) {
      a.val = b
    }), a.readonly = !1, b.readonly && a.$parent.$watch(c(b.readonly), function(b) {
      a.readonly = !! b
    })
  }
]).directive("rating", function() {
  return {
    restrict: "EA",
    scope: {
      value: "=",
      onHover: "&",
      onLeave: "&"
    },
    controller: "RatingController",
    templateUrl: "template/rating/rating.html",
    replace: !0
  }
}), angular.module("ui.bootstrap.tabs", []).controller("TabsetController", ["$scope",
  function(a) {
    var b = this,
      c = b.tabs = a.tabs = [];
    b.select = function(a) {
      angular.forEach(c, function(a) {
        a.active = !1
      }), a.active = !0
    }, b.addTab = function(a) {
      c.push(a), (1 === c.length || a.active) && b.select(a)
    }, b.removeTab = function(a) {
      var d = c.indexOf(a);
      if (a.active && c.length > 1) {
        var e = d == c.length - 1 ? d - 1 : d + 1;
        b.select(c[e])
      }
      c.splice(d, 1)
    }
  }
]).directive("tabset", function() {
  return {
    restrict: "EA",
    transclude: !0,
    replace: !0,
    scope: {},
    controller: "TabsetController",
    templateUrl: "template/tabs/tabset.html",
    link: function(a, b, c) {
      a.vertical = angular.isDefined(c.vertical) ? a.$parent.$eval(c.vertical) : !1, a.justified = angular.isDefined(c.justified) ? a.$parent.$eval(c.justified) : !1, a.type = angular.isDefined(c.type) ? a.$parent.$eval(c.type) : "tabs"
    }
  }
}).directive("tab", ["$parse",
  function(a) {
    return {
      require: "^tabset",
      restrict: "EA",
      replace: !0,
      templateUrl: "template/tabs/tab.html",
      transclude: !0,
      scope: {
        heading: "@",
        onSelect: "&select",
        onDeselect: "&deselect"
      },
      controller: function() {},
      compile: function(b, c, d) {
        return function(b, c, e, f) {
          var g, h;
          e.active ? (g = a(e.active), h = g.assign, b.$parent.$watch(g, function(a, c) {
            a !== c && (b.active = !! a)
          }), b.active = g(b.$parent)) : h = g = angular.noop, b.$watch("active", function(a) {
            h(b.$parent, a), a ? (f.select(b), b.onSelect()) : b.onDeselect()
          }), b.disabled = !1, e.disabled && b.$parent.$watch(a(e.disabled), function(a) {
            b.disabled = !! a
          }), b.select = function() {
            b.disabled || (b.active = !0)
          }, f.addTab(b), b.$on("$destroy", function() {
            f.removeTab(b)
          }), b.$transcludeFn = d
        }
      }
    }
  }
]).directive("tabHeadingTransclude", [
  function() {
    return {
      restrict: "A",
      require: "^tab",
      link: function(a, b) {
        a.$watch("headingElement", function(a) {
          a && (b.html(""), b.append(a))
        })
      }
    }
  }
]).directive("tabContentTransclude", function() {
  function a(a) {
    return a.tagName && (a.hasAttribute("tab-heading") || a.hasAttribute("data-tab-heading") || "tab-heading" === a.tagName.toLowerCase() || "data-tab-heading" === a.tagName.toLowerCase())
  }
  return {
    restrict: "A",
    require: "^tabset",
    link: function(b, c, d) {
      var e = b.$eval(d.tabContentTransclude);
      e.$transcludeFn(e.$parent, function(b) {
        angular.forEach(b, function(b) {
          a(b) ? e.headingElement = b : c.append(b)
        })
      })
    }
  }
}), angular.module("ui.bootstrap.timepicker", []).constant("timepickerConfig", {
  hourStep: 1,
  minuteStep: 1,
  showMeridian: !0,
  meridians: null,
  readonlyInput: !1,
  mousewheel: !0
}).directive("timepicker", ["$parse", "$log", "timepickerConfig", "$locale",
  function(a, b, c, d) {
    return {
      restrict: "EA",
      require: "?^ngModel",
      replace: !0,
      scope: {},
      templateUrl: "template/timepicker/timepicker.html",
      link: function(e, f, g, h) {
        function i() {
          var a = parseInt(e.hours, 10),
            b = e.showMeridian ? a > 0 && 13 > a : a >= 0 && 24 > a;
          return b ? (e.showMeridian && (12 === a && (a = 0), e.meridian === q[1] && (a += 12)), a) : void 0
        }

        function j() {
          var a = parseInt(e.minutes, 10);
          return a >= 0 && 60 > a ? a : void 0
        }

        function k(a) {
          return angular.isDefined(a) && a.toString().length < 2 ? "0" + a : a
        }

        function l(a) {
          m(), h.$setViewValue(new Date(p)), n(a)
        }

        function m() {
          h.$setValidity("time", !0), e.invalidHours = !1, e.invalidMinutes = !1
        }

        function n(a) {
          var b = p.getHours(),
            c = p.getMinutes();
          e.showMeridian && (b = 0 === b || 12 === b ? 12 : b % 12), e.hours = "h" === a ? b : k(b), e.minutes = "m" === a ? c : k(c), e.meridian = p.getHours() < 12 ? q[0] : q[1]
        }

        function o(a) {
          var b = new Date(p.getTime() + 6e4 * a);
          p.setHours(b.getHours(), b.getMinutes()), l()
        }
        if (h) {
          var p = new Date,
            q = angular.isDefined(g.meridians) ? e.$parent.$eval(g.meridians) : c.meridians || d.DATETIME_FORMATS.AMPMS,
            r = c.hourStep;
          g.hourStep && e.$parent.$watch(a(g.hourStep), function(a) {
            r = parseInt(a, 10)
          });
          var s = c.minuteStep;
          g.minuteStep && e.$parent.$watch(a(g.minuteStep), function(a) {
            s = parseInt(a, 10)
          }), e.showMeridian = c.showMeridian, g.showMeridian && e.$parent.$watch(a(g.showMeridian), function(a) {
            if (e.showMeridian = !! a, h.$error.time) {
              var b = i(),
                c = j();
              angular.isDefined(b) && angular.isDefined(c) && (p.setHours(b), l())
            } else n()
          });
          var t = f.find("input"),
            u = t.eq(0),
            v = t.eq(1),
            w = angular.isDefined(g.mousewheel) ? e.$eval(g.mousewheel) : c.mousewheel;
          if (w) {
            var x = function(a) {
              a.originalEvent && (a = a.originalEvent);
              var b = a.wheelDelta ? a.wheelDelta : -a.deltaY;
              return a.detail || b > 0
            };
            u.bind("mousewheel wheel", function(a) {
              e.$apply(x(a) ? e.incrementHours() : e.decrementHours()), a.preventDefault()
            }), v.bind("mousewheel wheel", function(a) {
              e.$apply(x(a) ? e.incrementMinutes() : e.decrementMinutes()), a.preventDefault()
            })
          }
          if (e.readonlyInput = angular.isDefined(g.readonlyInput) ? e.$eval(g.readonlyInput) : c.readonlyInput, e.readonlyInput) e.updateHours = angular.noop, e.updateMinutes = angular.noop;
          else {
            var y = function(a, b) {
              h.$setViewValue(null), h.$setValidity("time", !1), angular.isDefined(a) && (e.invalidHours = a), angular.isDefined(b) && (e.invalidMinutes = b)
            };
            e.updateHours = function() {
              var a = i();
              angular.isDefined(a) ? (p.setHours(a), l("h")) : y(!0)
            }, u.bind("blur", function() {
              !e.validHours && e.hours < 10 && e.$apply(function() {
                e.hours = k(e.hours)
              })
            }), e.updateMinutes = function() {
              var a = j();
              angular.isDefined(a) ? (p.setMinutes(a), l("m")) : y(void 0, !0)
            }, v.bind("blur", function() {
              !e.invalidMinutes && e.minutes < 10 && e.$apply(function() {
                e.minutes = k(e.minutes)
              })
            })
          }
          h.$render = function() {
            var a = h.$modelValue ? new Date(h.$modelValue) : null;
            isNaN(a) ? (h.$setValidity("time", !1), b.error('Timepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.')) : (a && (p = a), m(), n())
          }, e.incrementHours = function() {
            o(60 * r)
          }, e.decrementHours = function() {
            o(60 * -r)
          }, e.incrementMinutes = function() {
            o(s)
          }, e.decrementMinutes = function() {
            o(-s)
          }, e.toggleMeridian = function() {
            o(720 * (p.getHours() < 12 ? 1 : -1))
          }
        }
      }
    }
  }
]), angular.module("ui.bootstrap.typeahead", ["ui.bootstrap.position", "ui.bootstrap.bindHtml"]).factory("typeaheadParser", ["$parse",
  function(a) {
    var b = /^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;
    return {
      parse: function(c) {
        var d = c.match(b);
        if (!d) throw new Error("Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_' but got '" + c + "'.");
        return {
          itemName: d[3],
          source: a(d[4]),
          viewMapper: a(d[2] || d[1]),
          modelMapper: a(d[1])
        }
      }
    }
  }
]).directive("typeahead", ["$compile", "$parse", "$q", "$timeout", "$document", "$position", "typeaheadParser",
  function(a, b, c, d, e, f, g) {
    var h = [9, 13, 27, 38, 40];
    return {
      require: "ngModel",
      link: function(i, j, k, l) {
        var m, n = i.$eval(k.typeaheadMinLength) || 1,
          o = i.$eval(k.typeaheadWaitMs) || 0,
          p = i.$eval(k.typeaheadEditable) !== !1,
          q = b(k.typeaheadLoading).assign || angular.noop,
          r = b(k.typeaheadOnSelect),
          s = k.typeaheadInputFormatter ? b(k.typeaheadInputFormatter) : void 0,
          t = k.typeaheadAppendToBody ? b(k.typeaheadAppendToBody) : !1,
          u = b(k.ngModel).assign,
          v = g.parse(k.typeahead),
          w = angular.element("<div typeahead-popup></div>");
        w.attr({
          matches: "matches",
          active: "activeIdx",
          select: "select(activeIdx)",
          query: "query",
          position: "position"
        }), angular.isDefined(k.typeaheadTemplateUrl) && w.attr("template-url", k.typeaheadTemplateUrl);
        var x = i.$new();
        i.$on("$destroy", function() {
          x.$destroy()
        });
        var y = function() {
          x.matches = [], x.activeIdx = -1
        }, z = function(a) {
            var b = {
              $viewValue: a
            };
            q(i, !0), c.when(v.source(i, b)).then(function(c) {
              if (a === l.$viewValue && m) {
                if (c.length > 0) {
                  x.activeIdx = 0, x.matches.length = 0;
                  for (var d = 0; d < c.length; d++) b[v.itemName] = c[d], x.matches.push({
                    label: v.viewMapper(x, b),
                    model: c[d]
                  });
                  x.query = a, x.position = t ? f.offset(j) : f.position(j), x.position.top = x.position.top + j.prop("offsetHeight")
                } else y();
                q(i, !1)
              }
            }, function() {
              y(), q(i, !1)
            })
          };
        y(), x.query = void 0;
        var A;
        l.$parsers.unshift(function(a) {
          return m = !0, a && a.length >= n ? o > 0 ? (A && d.cancel(A), A = d(function() {
            z(a)
          }, o)) : z(a) : (q(i, !1), y()), p ? a : a ? (l.$setValidity("editable", !1), void 0) : (l.$setValidity("editable", !0), a)
        }), l.$formatters.push(function(a) {
          var b, c, d = {};
          return s ? (d.$model = a, s(i, d)) : (d[v.itemName] = a, b = v.viewMapper(i, d), d[v.itemName] = void 0, c = v.viewMapper(i, d), b !== c ? b : a)
        }), x.select = function(a) {
          var b, c, d = {};
          d[v.itemName] = c = x.matches[a].model, b = v.modelMapper(i, d), u(i, b), l.$setValidity("editable", !0), r(i, {
            $item: c,
            $model: b,
            $label: v.viewMapper(i, d)
          }), y(), j[0].focus()
        }, j.bind("keydown", function(a) {
          0 !== x.matches.length && -1 !== h.indexOf(a.which) && (a.preventDefault(), 40 === a.which ? (x.activeIdx = (x.activeIdx + 1) % x.matches.length, x.$digest()) : 38 === a.which ? (x.activeIdx = (x.activeIdx ? x.activeIdx : x.matches.length) - 1, x.$digest()) : 13 === a.which || 9 === a.which ? x.$apply(function() {
            x.select(x.activeIdx)
          }) : 27 === a.which && (a.stopPropagation(), y(), x.$digest()))
        }), j.bind("blur", function() {
          m = !1
        });
        var B = function(a) {
          j[0] !== a.target && (y(), x.$digest())
        };
        e.bind("click", B), i.$on("$destroy", function() {
          e.unbind("click", B)
        });
        var C = a(w)(x);
        t ? e.find("body").append(C) : j.after(C)
      }
    }
  }
]).directive("typeaheadPopup", function() {
  return {
    restrict: "EA",
    scope: {
      matches: "=",
      query: "=",
      active: "=",
      position: "=",
      select: "&"
    },
    replace: !0,
    templateUrl: "template/typeahead/typeahead-popup.html",
    link: function(a, b, c) {
      a.templateUrl = c.templateUrl, a.isOpen = function() {
        return a.matches.length > 0
      }, a.isActive = function(b) {
        return a.active == b
      }, a.selectActive = function(b) {
        a.active = b
      }, a.selectMatch = function(b) {
        a.select({
          activeIdx: b
        })
      }
    }
  }
}).directive("typeaheadMatch", ["$http", "$templateCache", "$compile", "$parse",
  function(a, b, c, d) {
    return {
      restrict: "EA",
      scope: {
        index: "=",
        match: "=",
        query: "="
      },
      link: function(e, f, g) {
        var h = d(g.templateUrl)(e.$parent) || "template/typeahead/typeahead-match.html";
        a.get(h, {
          cache: b
        }).success(function(a) {
          f.replaceWith(c(a.trim())(e))
        })
      }
    }
  }
]).filter("typeaheadHighlight", function() {
  function a(a) {
    return a.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1")
  }
  return function(b, c) {
    return c ? b.replace(new RegExp(a(c), "gi"), "<strong>$&</strong>") : b
  }
}), angular.module("template/accordion/accordion-group.html", []).run(["$templateCache",
  function(a) {
    a.put("template/accordion/accordion-group.html", '<div class="panel panel-default">\n  <div class="panel-heading">\n    <h4 class="panel-title">\n      <a class="accordion-toggle" ng-click="isOpen = !isOpen" accordion-transclude="heading">{{heading}}</a>\n    </h4>\n  </div>\n  <div class="panel-collapse" collapse="!isOpen">\n	  <div class="panel-body" ng-transclude></div>\n  </div>\n</div>')
  }
]), angular.module("template/accordion/accordion.html", []).run(["$templateCache",
  function(a) {
    a.put("template/accordion/accordion.html", '<div class="panel-group" ng-transclude></div>')
  }
]), angular.module("template/alert/alert.html", []).run(["$templateCache",
  function(a) {
    a.put("template/alert/alert.html", "<div class='alert' ng-class='\"alert-\" + (type || \"warning\")'>\n    <button ng-show='closeable' type='button' class='close' ng-click='close()'>&times;</button>\n    <div ng-transclude></div>\n</div>\n")
  }
]), angular.module("template/carousel/carousel.html", []).run(["$templateCache",
  function(a) {
    a.put("template/carousel/carousel.html", '<div ng-mouseenter="pause()" ng-mouseleave="play()" class="carousel">\n    <ol class="carousel-indicators" ng-show="slides().length > 1">\n        <li ng-repeat="slide in slides()" ng-class="{active: isActive(slide)}" ng-click="select(slide)"></li>\n    </ol>\n    <div class="carousel-inner" ng-transclude></div>\n    <a class="left carousel-control" ng-click="prev()" ng-show="slides().length > 1"><span class="icon-prev"></span></a>\n    <a class="right carousel-control" ng-click="next()" ng-show="slides().length > 1"><span class="icon-next"></span></a>\n</div>\n')
  }
]), angular.module("template/carousel/slide.html", []).run(["$templateCache",
  function(a) {
    a.put("template/carousel/slide.html", "<div ng-class=\"{\n    'active': leaving || (active && !entering),\n    'prev': (next || active) && direction=='prev',\n    'next': (next || active) && direction=='next',\n    'right': direction=='prev',\n    'left': direction=='next'\n  }\" class=\"item text-center\" ng-transclude></div>\n")
  }
]), angular.module("template/datepicker/datepicker.html", []).run(["$templateCache",
  function(a) {
    a.put("template/datepicker/datepicker.html", '<table>\n  <thead>\n    <tr>\n      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n      <th colspan="{{rows[0].length - 2 + showWeekNumbers}}"><button type="button" class="btn btn-default btn-sm btn-block" ng-click="toggleMode()"><strong>{{title}}</strong></button></th>\n      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n    </tr>\n    <tr ng-show="labels.length > 0" class="h6">\n      <th ng-show="showWeekNumbers" class="text-center">#</th>\n      <th ng-repeat="label in labels" class="text-center">{{label}}</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr ng-repeat="row in rows">\n      <td ng-show="showWeekNumbers" class="text-center"><em>{{ getWeekNumber(row) }}</em></td>\n      <td ng-repeat="dt in row" class="text-center">\n        <button type="button" style="width:100%;" class="btn btn-default btn-sm" ng-class="{\'btn-info\': dt.selected}" ng-click="select(dt.date)" ng-disabled="dt.disabled"><span ng-class="{\'text-muted\': dt.secondary}">{{dt.label}}</span></button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n')
  }
]), angular.module("template/datepicker/popup.html", []).run(["$templateCache",
  function(a) {
    a.put("template/datepicker/popup.html", "<ul class=\"dropdown-menu\" ng-style=\"{display: (isOpen && 'block') || 'none', top: position.top+'px', left: position.left+'px'}\">\n	<li ng-transclude></li>\n" + '	<li ng-show="showButtonBar" style="padding:10px 9px 2px">\n		<span class="btn-group">\n			<button type="button" class="btn btn-sm btn-info" ng-click="today()">{{currentText}}</button>\n			<button type="button" class="btn btn-sm btn-default" ng-click="showWeeks = ! showWeeks" ng-class="{active: showWeeks}">{{toggleWeeksText}}</button>\n			<button type="button" class="btn btn-sm btn-danger" ng-click="clear()">{{clearText}}</button>\n		</span>\n		<button type="button" class="btn btn-sm btn-success pull-right" ng-click="isOpen = false">{{closeText}}</button>\n	</li>\n</ul>\n')
  }
]), angular.module("template/modal/backdrop.html", []).run(["$templateCache",
  function(a) {
    a.put("template/modal/backdrop.html", '<div class="modal-backdrop fade" ng-class="{in: animate}" ng-style="{\'z-index\': 1040 + index*10}"></div>')
  }
]), angular.module("template/modal/window.html", []).run(["$templateCache",
  function(a) {
    a.put("template/modal/window.html", '<div tabindex="-1" class="modal fade {{ windowClass }}" ng-class="{in: animate}" ng-style="{\'z-index\': 1050 + index*10, display: \'block\'}" ng-click="close($event)">\n    <div class="modal-dialog"><div class="modal-content" ng-transclude></div></div>\n</div>')
  }
]), angular.module("template/pagination/pager.html", []).run(["$templateCache",
  function(a) {
    a.put("template/pagination/pager.html", '<ul class="pager">\n  <li ng-repeat="page in pages" ng-class="{disabled: page.disabled, previous: page.previous, next: page.next}"><a ng-click="selectPage(page.number)">{{page.text}}</a></li>\n</ul>')
  }
]), angular.module("template/pagination/pagination.html", []).run(["$templateCache",
  function(a) {
    a.put("template/pagination/pagination.html", '<ul class="pagination">\n  <li ng-repeat="page in pages" ng-class="{active: page.active, disabled: page.disabled}"><a ng-click="selectPage(page.number)">{{page.text}}</a></li>\n</ul>')
  }
]), angular.module("template/tooltip/tooltip-html-unsafe-popup.html", []).run(["$templateCache",
  function(a) {
    a.put("template/tooltip/tooltip-html-unsafe-popup.html", '<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" bind-html-unsafe="content"></div>\n</div>\n')
  }
]), angular.module("template/tooltip/tooltip-popup.html", []).run(["$templateCache",
  function(a) {
    a.put("template/tooltip/tooltip-popup.html", '<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="tooltip-arrow"></div>\n  <div class="tooltip-inner" ng-bind="content"></div>\n</div>\n')
  }
]), angular.module("template/popover/popover.html", []).run(["$templateCache",
  function(a) {
    a.put("template/popover/popover.html", '<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="arrow"></div>\n\n  <div class="popover-inner">\n      <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>\n      <div class="popover-content" ng-bind="content"></div>\n  </div>\n</div>\n')
  }
]), angular.module("template/progressbar/bar.html", []).run(["$templateCache",
  function(a) {
    a.put("template/progressbar/bar.html", '<div class="progress-bar" ng-class="type && \'progress-bar-\' + type" ng-transclude></div>')
  }
]), angular.module("template/progressbar/progress.html", []).run(["$templateCache",
  function(a) {
    a.put("template/progressbar/progress.html", '<div class="progress" ng-transclude></div>')
  }
]), angular.module("template/progressbar/progressbar.html", []).run(["$templateCache",
  function(a) {
    a.put("template/progressbar/progressbar.html", '<div class="progress"><div class="progress-bar" ng-class="type && \'progress-bar-\' + type" ng-transclude></div></div>')
  }
]), angular.module("template/rating/rating.html", []).run(["$templateCache",
  function(a) {
    a.put("template/rating/rating.html", '<span ng-mouseleave="reset()">\n    <i ng-repeat="r in range" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="glyphicon" ng-class="$index < val && (r.stateOn || \'glyphicon-star\') || (r.stateOff || \'glyphicon-star-empty\')"></i>\n</span>')
  }
]), angular.module("template/tabs/tab.html", []).run(["$templateCache",
  function(a) {
    a.put("template/tabs/tab.html", '<li ng-class="{active: active, disabled: disabled}">\n  <a ng-click="select()" tab-heading-transclude>{{heading}}</a>\n</li>\n')
  }
]), angular.module("template/tabs/tabset-titles.html", []).run(["$templateCache",
  function(a) {
    a.put("template/tabs/tabset-titles.html", "<ul class=\"nav {{type && 'nav-' + type}}\" ng-class=\"{'nav-stacked': vertical}\">\n</ul>\n")
  }
]), angular.module("template/tabs/tabset.html", []).run(["$templateCache",
  function(a) {
    a.put("template/tabs/tabset.html", '\n<div class="tabbable">\n  <ul class="nav {{type && \'nav-\' + type}}" ng-class="{\'nav-stacked\': vertical, \'nav-justified\': justified}" ng-transclude></ul>\n  <div class="tab-content">\n    <div class="tab-pane" \n         ng-repeat="tab in tabs" \n         ng-class="{active: tab.active}"\n         tab-content-transclude="tab">\n    </div>\n  </div>\n</div>\n')
  }
]), angular.module("template/timepicker/timepicker.html", []).run(["$templateCache",
  function(a) {
    a.put("template/timepicker/timepicker.html", '<table>\n	<tbody>\n		<tr class="text-center">\n			<td><a ng-click="incrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n			<td>&nbsp;</td>\n			<td><a ng-click="incrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n			<td ng-show="showMeridian"></td>\n		</tr>\n		<tr>\n			<td style="width:50px;" class="form-group" ng-class="{\'has-error\': invalidHours}">\n				<input type="text" ng-model="hours" ng-change="updateHours()" class="form-control text-center" ng-mousewheel="incrementHours()" ng-readonly="readonlyInput" maxlength="2">\n			</td>\n			<td>:</td>\n			<td style="width:50px;" class="form-group" ng-class="{\'has-error\': invalidMinutes}">\n				<input type="text" ng-model="minutes" ng-change="updateMinutes()" class="form-control text-center" ng-readonly="readonlyInput" maxlength="2">\n			</td>\n			<td ng-show="showMeridian"><button type="button" class="btn btn-default text-center" ng-click="toggleMeridian()">{{meridian}}</button></td>\n		</tr>\n		<tr class="text-center">\n			<td><a ng-click="decrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n			<td>&nbsp;</td>\n			<td><a ng-click="decrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n			<td ng-show="showMeridian"></td>\n		</tr>\n	</tbody>\n</table>\n')
  }
]), angular.module("template/typeahead/typeahead-match.html", []).run(["$templateCache",
  function(a) {
    a.put("template/typeahead/typeahead-match.html", '<a tabindex="-1" bind-html-unsafe="match.label | typeaheadHighlight:query"></a>')
  }
]), angular.module("template/typeahead/typeahead-popup.html", []).run(["$templateCache",
  function(a) {
    a.put("template/typeahead/typeahead-popup.html", "<ul class=\"dropdown-menu\" ng-style=\"{display: isOpen()&&'block' || 'none', top: position.top+'px', left: position.left+'px'}\">\n" + '    <li ng-repeat="match in matches" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)" ng-click="selectMatch($index)">\n        <div typeahead-match index="$index" match="match" query="query" template-url="templateUrl"></div>\n    </li>\n</ul>')
  }
]);;
/**
 * State-based routing for AngularJS
 * @version v0.2.8
 * @link http://angular-ui.github.com/
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
"undefined" != typeof module && "undefined" != typeof exports && module.exports === exports && (module.exports = "ui.router"),
function(a, b, c) {
  "use strict";

  function d(a, b) {
    return H(new(H(function() {}, {
      prototype: a
    })), b)
  }

  function e(a) {
    return G(arguments, function(b) {
      b !== a && G(b, function(b, c) {
        a.hasOwnProperty(c) || (a[c] = b)
      })
    }), a
  }

  function f(a, b) {
    var c = [];
    for (var d in a.path) {
      if (a.path[d] !== b.path[d]) break;
      c.push(a.path[d])
    }
    return c
  }

  function g(a, b) {
    if (Array.prototype.indexOf) return a.indexOf(b, Number(arguments[2]) || 0);
    var c = a.length >>> 0,
      d = Number(arguments[2]) || 0;
    for (d = 0 > d ? Math.ceil(d) : Math.floor(d), 0 > d && (d += c); c > d; d++)
      if (d in a && a[d] === b) return d;
    return -1
  }

  function h(a, b, c, d) {
    var e, h = f(c, d),
      i = {}, j = [];
    for (var k in h)
      if (h[k].params && h[k].params.length) {
        e = h[k].params;
        for (var l in e) g(j, e[l]) >= 0 || (j.push(e[l]), i[e[l]] = a[e[l]])
      }
    return H({}, i, b)
  }

  function i(a, b) {
    var c = {};
    return G(a, function(a) {
      var d = b[a];
      c[a] = null != d ? String(d) : null
    }), c
  }

  function j(a, b, c) {
    if (!c) {
      c = [];
      for (var d in a) c.push(d)
    }
    for (var e = 0; e < c.length; e++) {
      var f = c[e];
      if (a[f] != b[f]) return !1
    }
    return !0
  }

  function k(a, b) {
    var c = {};
    return G(a, function(a) {
      c[a] = b[a]
    }), c
  }

  function l(a, b) {
    var d = 1,
      f = 2,
      g = {}, h = [],
      i = g,
      j = H(a.when(g), {
        $$promises: g,
        $$values: g
      });
    this.study = function(g) {
      function k(a, c) {
        if (o[c] !== f) {
          if (n.push(c), o[c] === d) throw n.splice(0, n.indexOf(c)), new Error("Cyclic dependency: " + n.join(" -> "));
          if (o[c] = d, D(a)) m.push(c, [
            function() {
              return b.get(a)
            }
          ], h);
          else {
            var e = b.annotate(a);
            G(e, function(a) {
              a !== c && g.hasOwnProperty(a) && k(g[a], a)
            }), m.push(c, a, e)
          }
          n.pop(), o[c] = f
        }
      }

      function l(a) {
        return E(a) && a.then && a.$$promises
      }
      if (!E(g)) throw new Error("'invocables' must be an object");
      var m = [],
        n = [],
        o = {};
      return G(g, k), g = n = o = null,
      function(d, f, g) {
        function h() {
          --s || (t || e(r, f.$$values), p.$$values = r, p.$$promises = !0, o.resolve(r))
        }

        function k(a) {
          p.$$failure = a, o.reject(a)
        }

        function n(c, e, f) {
          function i(a) {
            l.reject(a), k(a)
          }

          function j() {
            if (!B(p.$$failure)) try {
              l.resolve(b.invoke(e, g, r)), l.promise.then(function(a) {
                r[c] = a, h()
              }, i)
            } catch (a) {
              i(a)
            }
          }
          var l = a.defer(),
            m = 0;
          G(f, function(a) {
            q.hasOwnProperty(a) && !d.hasOwnProperty(a) && (m++, q[a].then(function(b) {
              r[a] = b, --m || j()
            }, i))
          }), m || j(), q[c] = l.promise
        }
        if (l(d) && g === c && (g = f, f = d, d = null), d) {
          if (!E(d)) throw new Error("'locals' must be an object")
        } else d = i; if (f) {
          if (!l(f)) throw new Error("'parent' must be a promise returned by $resolve.resolve()")
        } else f = j;
        var o = a.defer(),
          p = o.promise,
          q = p.$$promises = {}, r = H({}, d),
          s = 1 + m.length / 3,
          t = !1;
        if (B(f.$$failure)) return k(f.$$failure), p;
        f.$$values ? (t = e(r, f.$$values), h()) : (H(q, f.$$promises), f.then(h, k));
        for (var u = 0, v = m.length; v > u; u += 3) d.hasOwnProperty(m[u]) ? h() : n(m[u], m[u + 1], m[u + 2]);
        return p
      }
    }, this.resolve = function(a, b, c, d) {
      return this.study(a)(b, c, d)
    }
  }

  function m(a, b, c) {
    this.fromConfig = function(a, b, c) {
      return B(a.template) ? this.fromString(a.template, b) : B(a.templateUrl) ? this.fromUrl(a.templateUrl, b) : B(a.templateProvider) ? this.fromProvider(a.templateProvider, b, c) : null
    }, this.fromString = function(a, b) {
      return C(a) ? a(b) : a
    }, this.fromUrl = function(c, d) {
      return C(c) && (c = c(d)), null == c ? null : a.get(c, {
        cache: b
      }).then(function(a) {
        return a.data
      })
    }, this.fromProvider = function(a, b, d) {
      return c.invoke(a, null, d || {
        params: b
      })
    }
  }

  function n(a) {
    function b(b) {
      if (!/^\w+(-+\w+)*$/.test(b)) throw new Error("Invalid parameter name '" + b + "' in pattern '" + a + "'");
      if (f[b]) throw new Error("Duplicate parameter name '" + b + "' in pattern '" + a + "'");
      f[b] = !0, j.push(b)
    }

    function c(a) {
      return a.replace(/[\\\[\]\^$*+?.()|{}]/g, "\\$&")
    }
    var d, e = /([:*])(\w+)|\{(\w+)(?:\:((?:[^{}\\]+|\\.|\{(?:[^{}\\]+|\\.)*\})+))?\}/g,
      f = {}, g = "^",
      h = 0,
      i = this.segments = [],
      j = this.params = [];
    this.source = a;
    for (var k, l, m;
      (d = e.exec(a)) && (k = d[2] || d[3], l = d[4] || ("*" == d[1] ? ".*" : "[^/]*"), m = a.substring(h, d.index), !(m.indexOf("?") >= 0));) g += c(m) + "(" + l + ")", b(k), i.push(m), h = e.lastIndex;
    m = a.substring(h);
    var n = m.indexOf("?");
    if (n >= 0) {
      var o = this.sourceSearch = m.substring(n);
      m = m.substring(0, n), this.sourcePath = a.substring(0, h + n), G(o.substring(1).split(/[&?]/), b)
    } else this.sourcePath = a, this.sourceSearch = "";
    g += c(m) + "$", i.push(m), this.regexp = new RegExp(g), this.prefix = i[0]
  }

  function o() {
    this.compile = function(a) {
      return new n(a)
    }, this.isMatcher = function(a) {
      return E(a) && C(a.exec) && C(a.format) && C(a.concat)
    }, this.$get = function() {
      return this
    }
  }

  function p(a) {
    function b(a) {
      var b = /^\^((?:\\[^a-zA-Z0-9]|[^\\\[\]\^$*+?.()|{}]+)*)/.exec(a.source);
      return null != b ? b[1].replace(/\\(.)/g, "$1") : ""
    }

    function c(a, b) {
      return a.replace(/\$(\$|\d{1,2})/, function(a, c) {
        return b["$" === c ? 0 : Number(c)]
      })
    }

    function d(a, b, c) {
      if (!c) return !1;
      var d = a.invoke(b, b, {
        $match: c
      });
      return B(d) ? d : !0
    }
    var e = [],
      f = null;
    this.rule = function(a) {
      if (!C(a)) throw new Error("'rule' must be a function");
      return e.push(a), this
    }, this.otherwise = function(a) {
      if (D(a)) {
        var b = a;
        a = function() {
          return b
        }
      } else if (!C(a)) throw new Error("'rule' must be a function");
      return f = a, this
    }, this.when = function(e, f) {
      var g, h = D(f);
      if (D(e) && (e = a.compile(e)), !h && !C(f) && !F(f)) throw new Error("invalid 'handler' in when()");
      var i = {
        matcher: function(b, c) {
          return h && (g = a.compile(c), c = ["$match",
            function(a) {
              return g.format(a)
            }
          ]), H(function(a, e) {
            return d(a, c, b.exec(e.path(), e.search()))
          }, {
            prefix: D(b.prefix) ? b.prefix : ""
          })
        },
        regex: function(a, e) {
          if (a.global || a.sticky) throw new Error("when() RegExp must not be global or sticky");
          return h && (g = e, e = ["$match",
            function(a) {
              return c(g, a)
            }
          ]), H(function(b, c) {
            return d(b, e, a.exec(c.path()))
          }, {
            prefix: b(a)
          })
        }
      }, j = {
          matcher: a.isMatcher(e),
          regex: e instanceof RegExp
        };
      for (var k in j)
        if (j[k]) return this.rule(i[k](e, f));
      throw new Error("invalid 'what' in when()")
    }, this.$get = ["$location", "$rootScope", "$injector",
      function(a, b, c) {
        function d(b) {
          function d(b) {
            var d = b(c, a);
            return d ? (D(d) && a.replace().url(d), !0) : !1
          }
          if (!b || !b.defaultPrevented) {
            var g, h = e.length;
            for (g = 0; h > g; g++)
              if (d(e[g])) return;
            f && d(f)
          }
        }
        return b.$on("$locationChangeSuccess", d), {
          sync: function() {
            d()
          }
        }
      }
    ]
  }

  function q(a, e, f) {
    function g(a) {
      return 0 === a.indexOf(".") || 0 === a.indexOf("^")
    }

    function l(a, b) {
      var d = D(a),
        e = d ? a : a.name,
        f = g(e);
      if (f) {
        if (!b) throw new Error("No reference point given for path '" + e + "'");
        for (var h = e.split("."), i = 0, j = h.length, k = b; j > i; i++)
          if ("" !== h[i] || 0 !== i) {
            if ("^" !== h[i]) break;
            if (!k.parent) throw new Error("Path '" + e + "' not valid for state '" + b.name + "'");
            k = k.parent
          } else k = b;
        h = h.slice(i).join("."), e = k.name + (k.name && h ? "." : "") + h
      }
      var l = u[e];
      return !l || !d && (d || l !== a && l.self !== a) ? c : l
    }

    function m(a, b) {
      v[a] || (v[a] = []), v[a].push(b)
    }

    function n(b) {
      b = d(b, {
        self: b,
        resolve: b.resolve || {},
        toString: function() {
          return this.name
        }
      });
      var c = b.name;
      if (!D(c) || c.indexOf("@") >= 0) throw new Error("State must have a valid name");
      if (u.hasOwnProperty(c)) throw new Error("State '" + c + "'' is already defined");
      var e = -1 !== c.indexOf(".") ? c.substring(0, c.lastIndexOf(".")) : D(b.parent) ? b.parent : "";
      if (e && !u[e]) return m(e, b.self);
      for (var f in x) C(x[f]) && (b[f] = x[f](b, x.$delegates[f]));
      if (u[c] = b, !b[w] && b.url && a.when(b.url, ["$match", "$stateParams",
        function(a, c) {
          t.$current.navigable == b && j(a, c) || t.transitionTo(b, a, {
            location: !1
          })
        }
      ]), v[c])
        for (var g = 0; g < v[c].length; g++) n(v[c][g]);
      return b
    }

    function o(a, b) {
      return D(a) && !B(b) ? x[a] : C(b) && D(a) ? (x[a] && !x.$delegates[a] && (x.$delegates[a] = x[a]), x[a] = b, this) : this
    }

    function p(a, b) {
      return E(a) ? b = a : b.name = a, n(b), this
    }

    function q(a, e, g, m, n, o, p) {
      function q() {
        p.url() !== D && (p.url(D), p.replace())
      }

      function v(a, c, d, f, h) {
        var i = d ? c : k(a.params, c),
          j = {
            $stateParams: i
          };
        h.resolve = n.resolve(a.resolve, j, h.resolve, a);
        var l = [h.resolve.then(function(a) {
          h.globals = a
        })];
        return f && l.push(f), G(a.views, function(c, d) {
          var e = c.resolve && c.resolve !== a.resolve ? c.resolve : {};
          e.$template = [
            function() {
              return g.load(d, {
                view: c,
                locals: j,
                params: i,
                notify: !1
              }) || ""
            }
          ], l.push(n.resolve(e, j, h.resolve, a).then(function(f) {
            if (C(c.controllerProvider) || F(c.controllerProvider)) {
              var g = b.extend({}, e, j);
              f.$$controller = m.invoke(c.controllerProvider, null, g)
            } else f.$$controller = c.controller;
            f.$$state = a, h[d] = f
          }))
        }), e.all(l).then(function() {
          return h
        })
      }
      var x = e.reject(new Error("transition superseded")),
        y = e.reject(new Error("transition prevented")),
        z = e.reject(new Error("transition aborted")),
        A = e.reject(new Error("transition failed")),
        D = p.url();
      return s.locals = {
        resolve: null,
        globals: {
          $stateParams: {}
        }
      }, t = {
        params: {},
        current: s.self,
        $current: s,
        transition: null
      }, t.reload = function() {
        t.transitionTo(t.current, o, {
          reload: !0,
          inherit: !1,
          notify: !1
        })
      }, t.go = function(a, b, c) {
        return this.transitionTo(a, b, H({
          inherit: !0,
          relative: t.$current
        }, c))
      }, t.transitionTo = function(b, c, f) {
        c = c || {}, f = H({
          location: !0,
          inherit: !1,
          relative: null,
          notify: !0,
          reload: !1,
          $retry: !1
        }, f || {});
        var g, k = t.$current,
          n = t.params,
          u = k.path,
          C = l(b, f.relative);
        if (!B(C)) {
          var E = {
            to: b,
            toParams: c,
            options: f
          };
          if (g = a.$broadcast("$stateNotFound", E, k.self, n), g.defaultPrevented) return q(), z;
          if (g.retry) {
            if (f.$retry) return q(), A;
            var F = t.transition = e.when(g.retry);
            return F.then(function() {
              return F !== t.transition ? x : (E.options.$retry = !0, t.transitionTo(E.to, E.toParams, E.options))
            }, function() {
              return z
            }), q(), F
          }
          if (b = E.to, c = E.toParams, f = E.options, C = l(b, f.relative), !B(C)) {
            if (f.relative) throw new Error("Could not resolve '" + b + "' from state '" + f.relative + "'");
            throw new Error("No such state '" + b + "'")
          }
        }
        if (C[w]) throw new Error("Cannot transition to abstract state '" + b + "'");
        f.inherit && (c = h(o, c || {}, t.$current, C)), b = C;
        var G, J, K = b.path,
          L = s.locals,
          M = [];
        for (G = 0, J = K[G]; J && J === u[G] && j(c, n, J.ownParams) && !f.reload; G++, J = K[G]) L = M[G] = J.locals;
        if (r(b, k, L, f)) return b.self.reloadOnSearch !== !1 && q(), t.transition = null, e.when(t.current);
        if (c = i(b.params, c || {}), f.notify && (g = a.$broadcast("$stateChangeStart", b.self, c, k.self, n), g.defaultPrevented)) return q(), y;
        for (var N = e.when(L), O = G; O < K.length; O++, J = K[O]) L = M[O] = d(L), N = v(J, c, J === b, N, L);
        var P = t.transition = N.then(function() {
          var d, e, g;
          if (t.transition !== P) return x;
          for (d = u.length - 1; d >= G; d--) g = u[d], g.self.onExit && m.invoke(g.self.onExit, g.self, g.locals.globals), g.locals = null;
          for (d = G; d < K.length; d++) e = K[d], e.locals = M[d], e.self.onEnter && m.invoke(e.self.onEnter, e.self, e.locals.globals);
          if (t.transition !== P) return x;
          t.$current = b, t.current = b.self, t.params = c, I(t.params, o), t.transition = null;
          var h = b.navigable;
          return f.location && h && (p.url(h.url.format(h.locals.globals.$stateParams)), "replace" === f.location && p.replace()), f.notify && a.$broadcast("$stateChangeSuccess", b.self, c, k.self, n), D = p.url(), t.current
        }, function(d) {
          return t.transition !== P ? x : (t.transition = null, a.$broadcast("$stateChangeError", b.self, c, k.self, n, d), q(), e.reject(d))
        });
        return P
      }, t.is = function(a, d) {
        var e = l(a);
        return B(e) ? t.$current !== e ? !1 : B(d) && null !== d ? b.equals(o, d) : !0 : c
      }, t.includes = function(a, d) {
        var e = l(a);
        if (!B(e)) return c;
        if (!B(t.$current.includes[e.name])) return !1;
        var f = !0;
        return b.forEach(d, function(a, b) {
          B(o[b]) && o[b] === a || (f = !1)
        }), f
      }, t.href = function(a, b, c) {
        c = H({
          lossy: !0,
          inherit: !1,
          absolute: !1,
          relative: t.$current
        }, c || {});
        var d = l(a, c.relative);
        if (!B(d)) return null;
        b = h(o, b || {}, t.$current, d);
        var e = d && c.lossy ? d.navigable : d,
          g = e && e.url ? e.url.format(i(d.params, b || {})) : null;
        return !f.html5Mode() && g && (g = "#" + f.hashPrefix() + g), c.absolute && g && (g = p.protocol() + "://" + p.host() + (80 == p.port() || 443 == p.port() ? "" : ":" + p.port()) + (!f.html5Mode() && g ? "/" : "") + g), g
      }, t.get = function(a, b) {
        if (!B(a)) {
          var c = [];
          return G(u, function(a) {
            c.push(a.self)
          }), c
        }
        var d = l(a, b);
        return d && d.self ? d.self : null
      }, t
    }

    function r(a, b, c, d) {
      return a !== b || (c !== b.locals || d.reload) && a.self.reloadOnSearch !== !1 ? void 0 : !0
    }
    var s, t, u = {}, v = {}, w = "abstract",
      x = {
        parent: function(a) {
          if (B(a.parent) && a.parent) return l(a.parent);
          var b = /^(.+)\.[^.]+$/.exec(a.name);
          return b ? l(b[1]) : s
        },
        data: function(a) {
          return a.parent && a.parent.data && (a.data = a.self.data = H({}, a.parent.data, a.data)), a.data
        },
        url: function(a) {
          var b = a.url;
          if (D(b)) return "^" == b.charAt(0) ? e.compile(b.substring(1)) : (a.parent.navigable || s).url.concat(b);
          if (e.isMatcher(b) || null == b) return b;
          throw new Error("Invalid url '" + b + "' in state '" + a + "'")
        },
        navigable: function(a) {
          return a.url ? a : a.parent ? a.parent.navigable : null
        },
        params: function(a) {
          if (!a.params) return a.url ? a.url.parameters() : a.parent.params;
          if (!F(a.params)) throw new Error("Invalid params in state '" + a + "'");
          if (a.url) throw new Error("Both params and url specicified in state '" + a + "'");
          return a.params
        },
        views: function(a) {
          var b = {};
          return G(B(a.views) ? a.views : {
            "": a
          }, function(c, d) {
            d.indexOf("@") < 0 && (d += "@" + a.parent.name), b[d] = c
          }), b
        },
        ownParams: function(a) {
          if (!a.parent) return a.params;
          var b = {};
          G(a.params, function(a) {
            b[a] = !0
          }), G(a.parent.params, function(c) {
            if (!b[c]) throw new Error("Missing required parameter '" + c + "' in state '" + a.name + "'");
            b[c] = !1
          });
          var c = [];
          return G(b, function(a, b) {
            a && c.push(b)
          }), c
        },
        path: function(a) {
          return a.parent ? a.parent.path.concat(a) : []
        },
        includes: function(a) {
          var b = a.parent ? H({}, a.parent.includes) : {};
          return b[a.name] = !0, b
        },
        $delegates: {}
      };
    s = n({
      name: "",
      url: "^",
      views: null,
      "abstract": !0
    }), s.navigable = null, this.decorator = o, this.state = p, this.$get = q, q.$inject = ["$rootScope", "$q", "$view", "$injector", "$resolve", "$stateParams", "$location", "$urlRouter"]
  }

  function r() {
    function a(a, b) {
      return {
        load: function(c, d) {
          var e, f = {
              template: null,
              controller: null,
              view: null,
              locals: null,
              notify: !0,
              async: !0,
              params: {}
            };
          return d = H(f, d), d.view && (e = b.fromConfig(d.view, d.params, d.locals)), e && d.notify && a.$broadcast("$viewContentLoading", d), e
        }
      }
    }
    this.$get = a, a.$inject = ["$rootScope", "$templateFactory"]
  }

  function s() {
    var a = !1;
    this.useAnchorScroll = function() {
      a = !0
    }, this.$get = ["$anchorScroll", "$timeout",
      function(b, c) {
        return a ? b : function(a) {
          c(function() {
            a[0].scrollIntoView()
          }, 0, !1)
        }
      }
    ]
  }

  function t(a, c, d, e, f, g) {
    function h() {
      return e.has ? function(a) {
        return e.has(a) ? e.get(a) : null
      } : function(a) {
        try {
          return e.get(a)
        } catch (b) {
          return null
        }
      }
    }

    function i(a, b, c) {
      var d = function() {
        return {
          leave: function(a) {
            a.remove()
          },
          enter: function(a, b, c) {
            c.after(a)
          }
        }
      };
      if (m) return function(a) {
        return a ? {
          enter: function(a, b, c) {
            m.enter(a, null, c)
          },
          leave: function(a) {
            m.leave(a, function() {
              a.remove()
            })
          }
        } : d()
      };
      if (l) {
        var e = l && l(c, b);
        return function(a) {
          return a ? {
            enter: function(a, b) {
              e.enter(a, b)
            },
            leave: function(a) {
              e.leave(a.contents(), a)
            }
          } : d()
        }
      }
      return d
    }
    var j = !1,
      k = h(),
      l = k("$animator"),
      m = k("$animate"),
      n = {
        restrict: "ECA",
        compile: function(e, h) {
          var k = e.html(),
            l = !0,
            m = b.element(g[0].createComment(" ui-view-anchor ")),
            o = e.parent();
          return e.prepend(m),
          function(g) {
            function p() {
              s && (y(!0).leave(s), s = null), r && (r.$destroy(), r = null)
            }

            function q(h) {
              var i = a.$current && a.$current.locals[v];
              if (l && (l = !1, e.replaceWith(m)), !i) return p(), s = e.clone(), s.html(k), y(h).enter(s, o, m), r = g.$new(), c(s.contents())(r), void 0;
              if (i !== t) {
                p(), s = e.clone(), s.html(i.$template ? i.$template : k), y(!0).enter(s, o, m), s.data("$uiView", z), t = i, z.state = i.$$state;
                var j = c(s.contents());
                if (r = g.$new(), i.$$controller) {
                  i.$scope = r;
                  var n = d(i.$$controller, i);
                  s.children().data("$ngControllerController", n)
                }
                j(r), r.$emit("$viewContentLoaded"), w && r.$eval(w), b.isDefined(x) && x && !g.$eval(x) || f(s)
              }
            }
            var r, s, t, u = o.inheritedData("$uiView"),
              v = h[n.name] || h.name || "",
              w = h.onload || "",
              x = h.autoscroll,
              y = i(e, h, g);
            v.indexOf("@") < 0 && (v = v + "@" + (u ? u.state.name : ""));
            var z = {
              name: v,
              state: null
            }, A = function() {
                if (!j) {
                  j = !0;
                  try {
                    q(!0)
                  } catch (a) {
                    throw j = !1, a
                  }
                  j = !1
                }
              };
            g.$on("$stateChangeSuccess", A), g.$on("$viewContentLoading", A), q(!1)
          }
        }
      };
    return n
  }

  function u(a) {
    var b = a.replace(/\n/g, " ").match(/^([^(]+?)\s*(\((.*)\))?$/);
    if (!b || 4 !== b.length) throw new Error("Invalid state ref '" + a + "'");
    return {
      state: b[1],
      paramExpr: b[3] || null
    }
  }

  function v(a) {
    var b = a.parent().inheritedData("$uiView");
    return b && b.state && b.state.name ? b.state : void 0
  }

  function w(a, b) {
    return {
      restrict: "A",
      require: "?^uiSrefActive",
      link: function(c, d, e, f) {
        var g = u(e.uiSref),
          h = null,
          i = v(d) || a.$current,
          j = "FORM" === d[0].nodeName,
          k = j ? "action" : "href",
          l = !0,
          m = function(b) {
            if (b && (h = b), l) {
              var c = a.href(g.state, h, {
                relative: i
              });
              return f && f.$$setStateInfo(g.state, h), c ? (d[0][k] = c, void 0) : (l = !1, !1)
            }
          };
        g.paramExpr && (c.$watch(g.paramExpr, function(a) {
          a !== h && m(a)
        }, !0), h = c.$eval(g.paramExpr)), m(), j || d.bind("click", function(c) {
          var e = c.which || c.button;
          0 !== e && 1 != e || c.ctrlKey || c.metaKey || c.shiftKey || d.attr("target") || (b(function() {
            a.go(g.state, h, {
              relative: i
            })
          }), c.preventDefault())
        })
      }
    }
  }

  function x(a, b, c) {
    return {
      restrict: "A",
      controller: ["$scope", "$element", "$attrs",
        function(d, e, f) {
          function g() {
            a.$current.self === i && h() ? e.addClass(l) : e.removeClass(l)
          }

          function h() {
            return !k || j(k, b)
          }
          var i, k, l;
          l = c(f.uiSrefActive || "", !1)(d), this.$$setStateInfo = function(b, c) {
            i = a.get(b, v(e)), k = c, g()
          }, d.$on("$stateChangeSuccess", g)
        }
      ]
    }
  }

  function y(a) {
    return function(b) {
      return a.is(b)
    }
  }

  function z(a) {
    return function(b) {
      return a.includes(b)
    }
  }

  function A(a, b) {
    function e(a) {
      this.locals = a.locals.globals, this.params = this.locals.$stateParams
    }

    function f() {
      this.locals = null, this.params = null
    }

    function g(c, g) {
      if (null != g.redirectTo) {
        var h, j = g.redirectTo;
        if (D(j)) h = j;
        else {
          if (!C(j)) throw new Error("Invalid 'redirectTo' in when()");
          h = function(a, b) {
            return j(a, b.path(), b.search())
          }
        }
        b.when(c, h)
      } else a.state(d(g, {
        parent: null,
        name: "route:" + encodeURIComponent(c),
        url: c,
        onEnter: e,
        onExit: f
      }));
      return i.push(g), this
    }

    function h(a, b, d) {
      function e(a) {
        return "" !== a.name ? a : c
      }
      var f = {
        routes: i,
        params: d,
        current: c
      };
      return b.$on("$stateChangeStart", function(a, c, d, f) {
        b.$broadcast("$routeChangeStart", e(c), e(f))
      }), b.$on("$stateChangeSuccess", function(a, c, d, g) {
        f.current = e(c), b.$broadcast("$routeChangeSuccess", e(c), e(g)), I(d, f.params)
      }), b.$on("$stateChangeError", function(a, c, d, f, g, h) {
        b.$broadcast("$routeChangeError", e(c), e(f), h)
      }), f
    }
    var i = [];
    e.$inject = ["$$state"], this.when = g, this.$get = h, h.$inject = ["$state", "$rootScope", "$routeParams"]
  }
  var B = b.isDefined,
    C = b.isFunction,
    D = b.isString,
    E = b.isObject,
    F = b.isArray,
    G = b.forEach,
    H = b.extend,
    I = b.copy;
  b.module("ui.router.util", ["ng"]), b.module("ui.router.router", ["ui.router.util"]), b.module("ui.router.state", ["ui.router.router", "ui.router.util"]), b.module("ui.router", ["ui.router.state"]), b.module("ui.router.compat", ["ui.router"]), l.$inject = ["$q", "$injector"], b.module("ui.router.util").service("$resolve", l), m.$inject = ["$http", "$templateCache", "$injector"], b.module("ui.router.util").service("$templateFactory", m), n.prototype.concat = function(a) {
    return new n(this.sourcePath + a + this.sourceSearch)
  }, n.prototype.toString = function() {
    return this.source
  }, n.prototype.exec = function(a, b) {
    var c = this.regexp.exec(a);
    if (!c) return null;
    var d, e = this.params,
      f = e.length,
      g = this.segments.length - 1,
      h = {};
    if (g !== c.length - 1) throw new Error("Unbalanced capture group in route '" + this.source + "'");
    for (d = 0; g > d; d++) h[e[d]] = c[d + 1];
    for (; f > d; d++) h[e[d]] = b[e[d]];
    return h
  }, n.prototype.parameters = function() {
    return this.params
  }, n.prototype.format = function(a) {
    var b = this.segments,
      c = this.params;
    if (!a) return b.join("");
    var d, e, f, g = b.length - 1,
      h = c.length,
      i = b[0];
    for (d = 0; g > d; d++) f = a[c[d]], null != f && (i += encodeURIComponent(f)), i += b[d + 1];
    for (; h > d; d++) f = a[c[d]], null != f && (i += (e ? "&" : "?") + c[d] + "=" + encodeURIComponent(f), e = !0);
    return i
  }, b.module("ui.router.util").provider("$urlMatcherFactory", o), p.$inject = ["$urlMatcherFactoryProvider"], b.module("ui.router.router").provider("$urlRouter", p), q.$inject = ["$urlRouterProvider", "$urlMatcherFactoryProvider", "$locationProvider"], b.module("ui.router.state").value("$stateParams", {}).provider("$state", q), r.$inject = [], b.module("ui.router.state").provider("$view", r), b.module("ui.router.state").provider("$uiViewScroll", s), t.$inject = ["$state", "$compile", "$controller", "$injector", "$uiViewScroll", "$document"], b.module("ui.router.state").directive("uiView", t), w.$inject = ["$state", "$timeout"], x.$inject = ["$state", "$stateParams", "$interpolate"], b.module("ui.router.state").directive("uiSref", w).directive("uiSrefActive", x), y.$inject = ["$state"], z.$inject = ["$state"], b.module("ui.router.state").filter("isState", y).filter("includedByState", z), A.$inject = ["$stateProvider", "$urlRouterProvider"], b.module("ui.router.compat").provider("$route", A).directive("ngView", t)
}(window, window.angular);;
app.factory('detailService', function($http, $rootScope, $q) {

  var pThresholdsObj;
  var parsedLists;

  return {

    getDetail: function(currentSub) {
      var parsedLists = $q.defer();
      var rowPromise = $http.get('http://livesearch.int.thomson.com/api/fuego/v1a/search?Sub_Id=' + currentSub);
      var notesPromise = $http.get('/get_notes');
      notesPromise.then(function(notesResponse) {
        var rawNotes = notesResponse.data;
        var returnedNotes = [];
        for (var i = 0; i < rawNotes.length; i++) {

          if ((currentSub * 1) === rawNotes[i].sub_id) {
            var tempNotesObj = {};
            tempNotesObj.activity = rawNotes[i].activity;
            tempNotesObj.author = rawNotes[i].author;
            tempNotesObj.content = rawNotes[i].content;
            tempNotesObj.date = rawNotes[i].date;
            tempNotesObj.safeID = rawNotes[i].safeID;
            tempNotesObj.subID = rawNotes[i].sub_id;
            tempNotesObj.rowData = JSON.parse(rawNotes[i].row_data);
            tempNotesObj["_id"] = rawNotes[i]["_id"];
            returnedNotes.push(tempNotesObj);
          }
          console.log(returnedNotes);

        }
        rowPromise.then(function(response) {
          console.log(response);
          var returnedData = response.data.results;
          //for(var i= 0; i<returnedData; )
          var newRows = [];
          console.log("objecto", returnedData);
          _.each(returnedData, function(project) {
            var tempRow = {};
            tempRow.project_age = project["#Project_Age_In_Days"];
            if (project.activities[0].Last_Name == "") {
              tempRow.participant_last_name = "Unassigned";
            } else {
              tempRow.participant_last_name = project.activities[0].Last_Name;
            }
            tempRow.participant_first_name = project.activities[0].First_Name;
            tempRow.org_unit = project.activities[0].Org_Unit;
            tempRow.pm_name = project.Project_Manager;
            tempRow.firm_name = project.Firm_Name;
            tempRow.wld_id = project.Wld_Id * 1;
            tempRow.sub_id = project.Sub_Id * 1;
            tempRow.logical_site = project.Logical_Site_Id * 1;
            tempRow.sap_type = project.Sap_Account_Type;
            if (project.Account_Value === "") {
              tempRow.Account_Value = "0.00";
            } else {
              tempRow.account_value = parseFloat(project.Account_Value);
            }

            tempRow.product = project.Product;
            tempRow.process_description = project.Process_Description;
            tempRow.activity_description = project.activities[0].Activity_Description;
            tempRow.activity_age = project.activities[0].Activity_Age_In_Business_Days;
            tempRow.activity_due = project.activities[0].Activity_Due_Date;
            tempRow.completed_date = project.Completed_Date;
            tempRow.status = project.Status;
            tempRow.bpm_id = project.Bpm_Project_Id * 1;
            tempRow.cdc = project.Cdc;
            tempRow.rsm = project.Rsm;
            tempRow.writer = project.Writer;
            tempRow.designer = project.Designer;
            tempRow.seo = project.Seo;
            tempRow.line_item_status = project.Line_Item_Status;
            tempRow.activity_status = project.activities[0].Activity_Status;
            tempRow.project_status = project.Project_Status;
            tempRow.priority = project.Priority;
            if (project.activities[0].Staff_Manager === "") {
              tempRow.staff_manager = "Unassigned";
            } else {
              tempRow.staff_manager = project.activities[0].Staff_Manager;

            }

            newRows.push(tempRow);

          });
          $http.get('/thresholds').then(function(response) {

            pThresholdsObj = parseThresholds(response.data);
            console.log(pThresholdsObj);
            var rows = parseRowsAssignColors(newRows, pThresholdsObj, [])
            parsedLists.resolve({
              "rows": rows, //this is a really bad solution
              "notes": returnedNotes

            });
          });
        });
      });
      return parsedLists.promise;
    }
  };
});


app.service('participantService', function($http, $q) {
  var parsedLists;
  var pThresholdsObj;
  return {

    getParticipant: function(participantFirst, participantLast) {
      var parsedLists = $q.defer();
      var rowPromise = $http.get('http://livesearch.int.thomson.com/api/fuego/v1a/search?First_Name' + participantFirst + '&Last_Name=' + participantLast).then(function(response) {
        var returnedData = response.data.results;
        //for(var i= 0; i<returnedData; )
        var newRows = transformServiceRows(returnedData)
        /*                _.each(returnedData, function(project) {

                    var tempRow = {};

                    tempRow.project_age = project["#Project_Age_In_Days"];
                    if (project.activities[0].Last_Name == "") {
                        tempRow.participant_last_name = "Unassigned";
                    } else {
                        tempRow.participant_last_name = project.activities[0].Last_Name;
                    }
                    tempRow.participant_first_name = project.activities[0].First_Name;
                    tempRow.org_unit = project.activities[0].Org_Unit;
                    tempRow.pm_name = project.Project_Manager;
                    tempRow.firm_name = project.Firm_Name;
                    tempRow.wld_id = project.Wld_Id * 1;
                    tempRow.sub_id = project.Sub_Id * 1;
                    tempRow.logical_site = project.Logical_Site_Id * 1;
                    tempRow.sap_type = project.Sap_Account_Type;
                    if (project.Account_Value === "") {
                        tempRow.Account_Value = "0.00";
                    } else {
                        tempRow.account_value = parseFloat(project.Account_Value);
                    }

                    tempRow.product = project.Product;
                    tempRow.process_description = project.Process_Description;
                    tempRow.activity_description = project.activities[0].Activity_Description;
                    tempRow.activity_age = project.activities[0].Activity_Age_In_Business_Days;
                    tempRow.activity_due = project.activities[0].Activity_Due_Date;
                    tempRow.completed_date = project.Completed_Date;
                    tempRow.status = project.Status;
                    tempRow.bpm_id = project.Bpm_Project_Id * 1;
                    tempRow.cdc = project.Cdc;
                    tempRow.rsm = project.Rsm;
                    tempRow.writer = project.Writer;
                    tempRow.designer = project.Designer;
                    tempRow.seo = project.Seo;
                    tempRow.line_item_status = project.Line_Item_Status;
                    tempRow.activity_status = project.activities[0].Activity_Status;
                    tempRow.project_status = project.Project_Status;
                    tempRow.priority = project.Priority;
                    if (project.activities[0].Staff_Manager === "") {
                        tempRow.staff_manager = "Unassigned";
                    } else {
                        tempRow.staff_manager = project.activities[0].Staff_Manager;

                    }

                    newRows.push(tempRow);

                });*/
        $http.get('/thresholds').then(function(response) {

          pThresholdsObj = parseThresholds(response.data);
          console.log(pThresholdsObj);

          parsedLists.resolve(parseRowsAssignColors(newRows, pThresholdsObj, []));

        });
      });

      return parsedLists.promise;
    }
  };
});

app.factory('thresholdServices', ['$http', '$rootScope',
  function($http, $rootScope) {
    $rootScope.newThresholdForm = [];
    return {
      getThresholds: function(callback) {
        $http.get('/thresholds').success(function(data) {
          $rootScope.rows = data;
        });
      },
      getActivitiesList: function(callback) {
        $http.get('/activity_list').success(function(data) {
          $rootScope.activityList = data;
        });
      }
    };
  }
]);

app.service('sharedProperties', function($http, $q) {
  var convertedOrgUnit = "Design";
  var thresholdsObj;
  var pThresholdsObj;
  var parsedLists;
  var recentTimestamp = '';
  var managerList = [];
  var activitiesList = [];
  var IsoDateModified = '';

  return {

    getRows: function(org) {
      parsedLists = $q.defer();
      convertedOrgUnit = convertOrgUnit(org);
      var rowPromise = $http.get('/group_request/' + convertedOrgUnit, {
        cache: false
      });
      $http.get('/thresholds').then(function(response) {

        pThresholdsObj = parseThresholds(response.data.results);
        rowPromise.then(function(nextResponse) {

          recentTimestamp = nextResponse.data.results[0]['timestamp']['$date'];
          parsedLists.resolve(parseRowsAssignColors(nextResponse.data, pThresholdsObj));



        });
      });
      return parsedLists.promise;
    },
    getThresholdsObj: function() {

      return parsedLists.promise;
    }
  };
});
app.service('userService', function($http, $q) {
  return {
    resetUserPrefs: function(user) {
      /*$http.get('/reset_user_prefs').then(function(response) {
                console.log(response);
            });*/
      var tempObj = {};
      tempObj.user_id = user.safe_id;
      console.log(tempObj);
      var returnData = $q.defer();
      var request = $http({
        url: '/reset_user_prefs',
        method: "POST",
        data: JSON.stringify(tempObj),
        headers: {
          'Content-Type': 'application/json'
        }
      });
      console.log(request);
      request.then(function(response) {
        returnData.resolve(response.data);

      });
      return returnData.promise;

    },
    getUser: function() {
      user = $q.defer();
      $http.get('/get_user').then(function(response) {
        user.resolve(response.data);
      });
      return user.promise;
    },
    getUserData: function() {
      userData = $q.defer();
      $http.get('/get_user_data').then(function(response) {
        userData.resolve(response.data);
      });
      return userData.promise;
    },
    setUserData: function(formData) {

      newUserData = {}
      returnData = $q.defer();
      if (formData.org != "" || formData.org != null) {
        newUserData["preferred_view"] = formData.org;
      } else {
        newUserData["preferredView"] = null;
      }
      if (formData.activity != "" || formData.activity != null) {
        newUserData["activity_filter"] = formData.activity;
      } else {
        newUserData["activity_filter"] = null;
      }
      if (formData.manager != "" || formData.manger != null) {
        newUserData["manager_filter"] = formData.manager;
      } else {
        newUserData["manager_filter"] = null;
      }

      var request = $http({
        url: '/update_user_data',
        method: "POST",
        data: JSON.stringify({
          pref_view: formData.org,
          pref_manager_filter: formData.manager,
          pref_activity_filter: formData.activity
        }),
        headers: {
          'Content-Type': 'application/json'
        }
      });
      request.then(function(response) {
        returnData.resolve(response.data);

      });
      return returnData.promise;

    }


  }
});
app.factory('tempListService', ['$http', '$rootScope',
  function($http, $rootScope) {
    return {
      getList: function(listType, orgUnit) {

        var processedOrgUnit = orgUnit.replace(/ /g, "");
        var nameConcat = listType + processedOrgUnit;
        var queryData = $http.get('/static/temp_json/' + nameConcat + '.json');
        return queryData;
      }
    };
  }
]);

app.service('tempLiveSearchService', function($http, $q) {
  var parsedLists;
  var pThresholdsObj;
  return {

    getData: function(query) {
      switch (query) {
        case "design":
          convertedOrgUnit = "Org_Unit=Design";
          break;

        case "development":
          convertedOrgUnit = "Org_Unit=FED+BGL&Org_Unit=FED+EGN";
          break;



        default:
          convertedOrgUnit = "Org_Unit=Design";
          break;

      }
      var notesPromise = $http.get('/get_notes');
      var parsedLists = $q.defer();

      //var rowPromise = $http.get('http://livesearch.int.thomson.com/api/fuego/activities?' + convertedOrgUnit);
      //new api url
      var rowPromise = $http.get('http://livesearch.int.thomson.com/api/fuego/v1a/search?' + convertedOrgUnit + "&limit=200");

      notesPromise.then(function(notesResponse) {
        var rawNotes = notesResponse.data;
        var returnedNotes = [];
        for (var i = 0; i < rawNotes.length; i++) {
          var tempNotesObj = {};
          tempNotesObj.activity = rawNotes[i].activity;
          tempNotesObj.author = rawNotes[i].author;
          tempNotesObj.content = rawNotes[i].content;
          tempNotesObj.date = rawNotes[i].date;
          tempNotesObj.safeID = rawNotes[i].safeID;
          tempNotesObj.subID = rawNotes[i].sub_id;
          tempNotesObj.rowData = JSON.parse(rawNotes[i].row_data);
          tempNotesObj["_id"] = rawNotes[i]["_id"];
          returnedNotes.push(tempNotesObj);

        }
        rowPromise.then(function(response) {
          console.log(response, "this is the response from the rows");
          var returnedData = response.data.results;
          //for(var i= 0; i<returnedData; )

          newRows = transformServiceRows(returnedData);

          $http.get('/thresholds').then(function(response) {

            pThresholdsObj = parseThresholds(response.data);
            console.log(pThresholdsObj);
            /*rowPromise.then(function(nextResponse) {
                           console.log(nextResponse);
                           recentTimestamp = nextResponse.data[0]['timestamp']['$date'];
                           parsedLists.resolve(parseRowsAssignColors(nextResponse.data, pThresholdsObj));



                           });*/
            parsedLists.resolve(parseRowsAssignColors(newRows, pThresholdsObj, returnedNotes));

          });
        });
      });



      return parsedLists.promise;
    },
    getDateModified: function() {
      var url = '/get_date_modified';
      return $http.get(url);
    }
  };
});
app.service("notesService", function($http, $q) {
  return {
    writeNote: function(noteObj) {
      var returnData = $q.defer();
      if (!noteObj) {
        returnData.resolve("error");
        return returnData.promise;
      }
      console.log(noteObj);
      var request = $http({
        url: '/write_note',
        method: "POST",
        data: JSON.stringify(noteObj),
        headers: {
          'Content-Type': 'application/json'
        }
      });
      request.then(function(response) {
        returnData.resolve(response.data);

      });
      return returnData.promise;

    },
    readNotes: function(sub_id) {
      var returnData = $q.defer();
      $http.get('/get_notes').then(function(response) {
        var rawNotes = response.data;
        var returnedNotes = [];
        for (var i = 0; i < rawNotes.length; i++) {
          var tempNotesObj = {};
          tempNotesObj.activity = rawNotes[i].activity;
          tempNotesObj.author = rawNotes[i].author;
          tempNotesObj.content = rawNotes[i].content;
          tempNotesObj.date = rawNotes[i].date;
          tempNotesObj.safeID = rawNotes[i].safeID;
          tempNotesObj.subID = rawNotes[i].sub_id;
          tempNotesObj.rowData = JSON.parse(rawNotes[i].row_data);
          tempNotesObj["_id"] = rawNotes[i]["_id"];
          returnedNotes.push(tempNotesObj);

        }


        console.log(returnedNotes);
        returnData.resolve(returnedNotes);
      });
      return returnData.promise;
    },
    updateNote: function(note, user) {


      console.log(user);
      if (user.safe_id !== note.safeID) {
        alert("you are not the author!");
        return "error";
      }
      console.log(note);
      var tempNotesObj = {};
      tempNotesObj.id = note["_id"];
      tempNotesObj.date = note.date;
      tempNotesObj.subID = note.subID;
      tempNotesObj.safeID = note.safeID;
      tempNotesObj.content = note.content;
      console.log(JSON.stringify(tempNotesObj));
      var request = $http({
        url: '/update_note',
        method: "POST",
        data: JSON.stringify(tempNotesObj),
        headers: {
          'Content-Type': 'application/json'
        }
      });
      return request;

    },

    deleteNote: function(id, user) {
      var tempObj = {};
      tempObj.safeID = user.safe_id;
      tempObj.note_id = id;
      console.log(tempObj);
      var request = $http({
        url: '/delete_note',
        method: 'POST',
        data: JSON.stringify(tempObj),
        headers: {
          'Content-Type': 'application/json'
        }
      });
      return request;
    }
  }

});;
var parseThresholds = function(thresholdsData) {

  var thresholdsObj = {};
  for (var i = 0; i < thresholdsData.length; i++) {

    tActivity = thresholdsData[i].activity;
    tGreenActivity = thresholdsData[i].green_activity;
    tYellowActivity = thresholdsData[i].yellow_activity;
    tGreenProject = thresholdsData[i].green_project;
    tYellowProject = thresholdsData[i].yellow_project;
    thresholdsObj[tActivity] = {
      activityGreen: tGreenActivity,
      activityYellow: tYellowActivity,
      projectGreen: tGreenProject,
      projectYellow: tYellowProject,
      activityGreenCount: 0,
      activityYellowCount: 0,
      activityRedCount: 0,
      projectGreenCount: 0,
      projectYellowCount: 0,
      projectRedCount: 0
    };

  }
  return thresholdsObj;
}
var convertOrgUnit = function(org) {

  switch (org) {
    case "Development":
      convertedOrgUnit = "FED BGL+FED EGN";
      break;

    default:
      convertedOrgUnit = org;
      break;

  }

  return convertedOrgUnit;
};
var parseRowsAssignColors = function(data, thresholdsObj, notesObject) {

  activitiesList = [];
  managersList = [];
  thresholdKeys = _.keys(thresholdsObj);
  newData = [];
  totalActivityRedCount = 0;
  totalActivityYellowCount = 0;
  totalActivityGreenCount = 0;
  totalProjectRedCount = 0;
  totalProjectYellowCount = 0;
  totalProjectGreenCount = 0;
  untrackedData = false;
  //building process hierarchy
  threshHieObj = {};
  productArr = [];


  for (i = 0; i < data.length; i++) {

    //create a list of all the activities on the row data
    if (!_.contains(activitiesList, data[i].activity_description)) {
      activitiesList.push(data[i].activity_description);
    }
    if (!_.contains(managersList, data[i].staff_manager)) {
      managersList.push(data[i].staff_manager);
    }
    var rawProduct = data[i].product;
    var tmpPrArr;
    /* if(rawProduct.indexOf('-') >= 0){
            tmpPrArr = rawProduct.split("-");
            console.log("splitting product with  -", rawProduct);
        } else {*/
    tmpPrArr = rawProduct.split(":");
    //console.log("splitting product with  :", rawProduct);
    /* }*/
    var product = tmpPrArr[0];
    data[i].short_product = product;

    if (!_.contains(productArr, product)) {
      productArr.push(product);
    }


    var actDesc = data[i].activity_description;
    if (!threshHieObj[product]) {
      threshHieObj[product] = {};
      console.log("added " + product);
      if (!threshHieObj[product][actDesc]) {
        threshHieObj[product][actDesc] = {
          "thresholds": {
            "activity": {
              "green": 0,
              "yellow": 0,
              "red": 0
            },
            "project": {
              "green": 0,
              "yellow": 0,
              "red": 0
            }
          }
        }
        console.log("added a " + actDesc + " threshold");
      }
    }



    if (data[i].activity_status == "Green") {
      data[i].activity_color = "green";
      totalActivityGreenCount++;
      if (_.contains(thresholdKeys, data[i].activity_description)) {
        thresholdsObj[data[i].activity_description].activityGreenCount++;
      }
    } else if (data[i].activity_status == "Yellow") {
      data[i].activity_color = "yellow";
      totalActivityYellowCount++;
      if (_.contains(thresholdKeys, data[i].activity_description)) {
        thresholdsObj[data[i].activity_description].activityYellowCount++;
      }

    } else if (data[i].activity_status == "Red") {
      data[i].activity_color = "red";
      totalActivityRedCount++;
      if (_.contains(thresholdKeys, data[i].activity_description)) {
        thresholdsObj[data[i].activity_description].activityRedCount++;
      }
    }
    if (_.contains(thresholdKeys, data[i].activity_description)) {
      if (data[i].project_age <= thresholdsObj[data[i].activity_description].projectGreen) {
        data[i].project_color = "green";
        totalProjectGreenCount++;
        thresholdsObj[data[i].activity_description].projectGreenCount++;
      } else if (data[i].project_age > thresholdsObj[data[i].activity_description].projectGreen && data[i].project_age <= thresholdsObj[data[i].activity_description].projectYellow) {
        data[i].project_color = "yellow";
        totalProjectYellowCount++;
        thresholdsObj[data[i].activity_description].projectYellowCount++;

      } else {
        data[i].project_color = "red";
        totalProjectRedCount++;
        thresholdsObj[data[i].activity_description].projectRedCount++;
      }


    } else {
      untrackedData = true;
    }
    data[i].notes = [];
    for (j = 0; j < notesObject.length; j++) {

      if (data[i].sub_id === notesObject[j].subID) {
        data[i].notes.push(notesObject[j]);
        console.log(data[i]);
      }
    }
    newData.push(data[i]);



  }
  console.log(threshHieObj);
  //console.log(JSON.stringify(threshHieObj));
  //console.log(totalActivityRedCount+totalActivityYellowCount+totalActivityGreenCount);
  return {
    thresholdsObj: thresholdsObj,
    activitiesList: activitiesList,
    managersList: managersList,
    untrackedData: untrackedData,
    productsList: productArr,
    threshActTotals: {
      'totalActRedCount': totalActivityRedCount,
      'totalActYellowCount': totalActivityYellowCount,
      'totalActGreenCount': totalActivityGreenCount
    },
    threshProjTotals: {
      'totalProjRedCount': totalProjectRedCount,
      'totalProjYellowCount': totalProjectYellowCount,
      'totalProjGreenCount': totalProjectGreenCount
    },
    rows: newData
  };

};
focusFilter = function(actColor, projColor) {

  var returnResult = true;
  switch ($scope.filterBy) {

    case "activity":
      returnResult = (actColor == $scope.actColorFilter) ? true : false;
      ////console.log("activity" + actColor + "|" + $scope.actColorFilter + "|" + returnResult);
      return returnResult;
      break;
    case "project":
      returnResult = (projColor == $scope.projColorFilter) ? true : false;
      return returnResult;
      break;
    default:
      ////console.log("none");
      return returnResult;
      break;
  }


};
var makeBothCharts = function(activity, thresholdsObj, threshActTotals, threshProjTotals, untrackedData) {
  //returns object with Activity array and Project array
  //console.log(threshActTotals);
  var projectPie;
  var activityPie;
  if (activity == "") {
    //use threshold totals instead of thresholds object for a spec. activity
    console.log("running make Both Charts");
    console.log(threshActTotals);
    activityPie = [
      ["On Time", threshActTotals.totalActGreenCount],
      ["Caution", threshActTotals.totalActYellowCount],
      ["Late", threshActTotals.totalActRedCount]
    ];

    //check to see if there is any untracked project data (activities w/o project data)
    // if there is, we won't make data - just return untracked
    if (untrackedData) {
      projectPie = "untracked";
      return {
        project: projectPie,
        activity: activityPie
      }
    } else {
      // this means that all activies listed have project thresholds, so we'll get the totals for initial view
      projectPie = [
        ["On Time", threshProjTotals.totalProjGreenCount],
        ["Caution", threshProjTotals.totalProjYellowCount],
        ["Late", threshProjTotals.totalProjRedCount]
      ];
      return {
        project: projectPie,
        activity: activityPie
      }
    }
  } else {
    // there is an activity defined, so make both pie charts using thresholds data
    activityPie = makeOnePieChart("activity", activity, thresholdsObj);
    projectPie = makeOnePieChart("project", activity, thresholdsObj);
    var activityArray = [];
    var projectArray = [];
    if (activityPie == "no data" || projectPie == "no data") {
      return "no data";
    }
    _.forEach(activityPie, function(value, key) {
      activityArray.push([key, value]);
    });

    _.forEach(projectPie, function(value, key) {
      projectArray.push([key, value]);
    });
    return {
      project: projectArray,
      activity: activityArray
    }

  }


};
var makeOnePieChart = function(type, activity, thresholdsObj) {
  // returns object for hicharts pie chart ! must be converted to an array to be used

  var pieCount;

  try {
    var tempVar = thresholdsObj[activity].projectGreenCount;

    switch (type) {
      case "activity":
        pieCount = {
          "On Time": thresholdsObj[activity].activityGreenCount,
          "Caution": thresholdsObj[activity].activityYellowCount,
          "Late": thresholdsObj[activity].activityRedCount
        };


        break;

      case "project":
        pieCount = {
          "On Time": thresholdsObj[activity].projectGreenCount,
          "Caution": thresholdsObj[activity].projectYellowCount,
          "Late": thresholdsObj[activity].projectRedCount
        };
    }

    return pieCount;
  } catch (err) {
    console.log("oops, we've got an error");
    console.log(err.message);
    return "no data";
  }

};
var transformServiceRows = function(rowArray) {
  var newRows = [];
  _.each(rowArray, function(project) {

    var tempRow = {};

    tempRow.project_age = project["#Project_Age_In_Days"];
    if (project.activities[0].Last_Name == "") {
      tempRow.participant_last_name = "Unassigned";
    } else {
      tempRow.participant_last_name = project.activities[0].Last_Name;
    }
    tempRow.participant_first_name = project.activities[0].First_Name;
    tempRow.org_unit = project.activities[0].Org_Unit;
    tempRow.pm_name = project.Project_Manager;
    tempRow.firm_name = project.Firm_Name;
    tempRow.wld_id = project.Wld_Id * 1;
    tempRow.sub_id = project.Sub_Id * 1;
    tempRow.logical_site = project.Logical_Site_Id * 1;
    tempRow.sap_type = project.Sap_Account_Type;
    if (project.Account_Value === "") {
      tempRow.Account_Value = "0.00";
    } else {
      tempRow.account_value = parseFloat(project.Account_Value);
    }

    tempRow.product = project.Product;
    tempRow.process_description = project.Process_Description;
    tempRow.activity_description = project.activities[0].Activity_Description;
    tempRow.activity_age = project.activities[0].Activity_Age_In_Business_Days;
    tempRow.activity_due = project.activities[0].Activity_Due_Date;
    tempRow.completed_date = project.Completed_Date;
    tempRow.status = project.Status;
    tempRow.bpm_id = project.Bpm_Project_Id * 1;
    tempRow.cdc = project.Cdc;
    tempRow.rsm = project.Rsm;
    tempRow.writer = project.Writer;
    tempRow.designer = project.Designer;
    tempRow.seo = project.Seo;
    tempRow.line_item_status = project.Line_Item_Status;
    tempRow.activity_status = project.activities[0].Activity_Status;
    tempRow.project_status = project.Project_Status;
    tempRow.priority = project.Priority;
    if (project.activities[0].Staff_Manager === "") {
      tempRow.staff_manager = "Unassigned";
    } else {
      tempRow.staff_manager = project.activities[0].Staff_Manager;

    }

    newRows.push(tempRow);

  });
  return newRows;
};

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
};
app.directive('hcPieProject', function() {
  return {
    restrict: 'C',
    replace: true,
    scope: {
      items: "="
    },

    template: '<div id="container2" style="margin: 0 auto">not working</div>',
    link: function($scope, element, attrs) {

      var chart = new Highcharts.Chart({
        chart: {
          renderTo: 'container2',
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          backgroundColor: 'transparent',
          animation: false
        },
        credits: {
          enabled: false
        },
        exporting: {
          enabled: false
        },
        tooltip: {
          enabled: false
        },
        plotOptions: {
          pie: {
            animation: {
              duration: 1500,
              //easing: 'easeOutBounce'
            },
            allowPointSelect: true,
            cursor: 'pointer',
            slicedOffset: 0,
            borderColor: 'none',
            dataLabels: {
              enabled: false,
            }
          }
        },
        series: [{
          type: 'pie',
          point: {
            events: {
              click: function(event) {

                switch (this.name) {
                  case "On Time":
                    $scope.$emit('COLOR_CHANGE', {
                      color: 'green',
                      type: 'project',
                      label: this.name
                    });
                    break;
                  case "Caution":
                    $scope.$emit('COLOR_CHANGE', {
                      color: 'yellow',
                      type: 'project',
                      label: this.name
                    });
                    break;
                  case "Late":
                    $scope.$emit('COLOR_CHANGE', {
                      color: 'red',
                      type: 'project',
                      label: this.name
                    });
                    break;
                  default:
                    alert("no entry");
                    break;
                }

              }
            }
          },
          name: 'Project Age',
          data: $scope.outputPieProject,
          colors: ["#61B257", "#ECD356", "#CC6666"]
        }]
      });


      $scope.$watch("items", function(newValue, oldValue) {

        chart.series[0].setData(newValue, true);
      }, true);

    }

  };
});

app.directive('hcPieActivity', function() {
  return {
    restrict: 'C',
    replace: true,
    scope: {
      items: "="


    },

    template: '<div id="container1" style="margin: 0 auto">not working</div>',
    link: function($scope, element, attrs) {

      var chart = new Highcharts.Chart({
        chart: {
          renderTo: 'container1',
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          backgroundColor: 'transparent',
          animation: true
        },
        credits: {
          enabled: false
        },
        exporting: {
          enabled: false
        },
        tooltip: {
          enabled: false
        },
        plotOptions: {

          pie: {

            allowPointSelect: true,
            cursor: 'pointer',
            slicedOffset: 0,
            borderColor: 'none',
            dataLabels: {
              enabled: false

            }


          }
        },
        series: [{
          type: 'pie',
          point: {
            events: {
              click: function(event) {
                $scope.actColorFilter = "";
                switch (this.name) {
                  case "On Time":

                    $scope.$emit('COLOR_CHANGE', {
                      color: 'green',
                      type: 'activity',
                      label: this.name
                    });
                    $scope.actColorFilter = 'green';
                    break;
                  case "Caution":
                    $scope.$emit('COLOR_CHANGE', {
                      color: 'yellow',
                      type: 'activity',
                      label: this.name
                    });
                    break;
                  case "Late":
                    $scope.$emit('COLOR_CHANGE', {
                      color: 'red',
                      type: 'activity',
                      label: this.name
                    });
                    break;
                }

              }
            }
          },
          name: 'Activity Age',
          data: $scope.outputPieActivity,
          colors: ["#61B257", "#ECD356", "#CC6666"]
        }]
      });
      $scope.$watch("items", function(newValue, oldValue) {

        chart.series[0].setData(newValue, true);
      }, true);


    }

  };
});;
var app = angular.module('Traffik', [
  'ui.router', 'ui.bootstrap.affix', 'ui.bootstrap', 'ui.bootstrap.tpls', 'ui.bootstrap.alert'
]);
/*
                                            
                                            
                                            
             ##           ##                
             ##           ##                
      ###  #####   ###  #####   ###    ###  
     ## ##  ##    #  ##  ##    ## ##  ## ## 
      ##    ##     ####  ##    #####   ##   
       ##  ##    ## ##  ##    ##        ##  
    ## ##  ##    ## ##  ##    ## ##  ## ##  
     ###    ##    ## ##  ##    ###    ###   
                                            
                                            
*/
app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('design', {
      url: '/design',
      templateUrl: 'static/partials/landing.html',
      controller: 'mainCtrl'

    })

  .state('development', {
    url: '/development',
    templateUrl: 'static/partials/landing.html',
    controller: 'mainCtrl'

  })

  .state('detail', {
    url: '/detail/:id',
    templateUrl: 'static/partials/detail.content.html',
    controller: 'detailCtrl'
  })
    .state('participant', {
      url: '/participant/:id',
      templateUrl: 'static/partials/participant.content.html',
      controller: 'participantCtrl'
    })
    .state('thresholds', {
      url: '/set-thresholds',
      templateUrl: 'static/partials/threshold.content.html',
      controller: 'thresholdsCtrl'
    });
});
app.run(['$rootScope', '$state', '$stateParams', 'userService',
  function($rootScope, $state, $stateParams, userService) {
    $rootScope.pref = {};
    userService.getUserData().then(function(response) {
      var pref_view = "";
      var lastViewed = sessionStorage.getItem("lastViewed");

      var lastViewedParamsString = sessionStorage.getItem("lastViewedParams");
      console.log("here they are", JSON.parse(lastViewedParamsString));
      if (lastViewedParamsString == '{"id":""}') {
        lastViewedParams = '';

        console.log(lastViewedParamsString);
      } else {

        lastViewedParams = JSON.parse(lastViewedParamsString);
      }


      $rootScope.userData = response;
      if (response.pref_view) {

        pref_view = response.pref_view.toLowerCase();
      }

      if (lastViewed != null || lastViewed != undefined) {

        console.log(lastViewedParams);

        $state.transitionTo(lastViewed, lastViewedParams);
      } else if (pref_view != "") {
        $state.transitionTo(pref_view);
      } else {
        $state.transitionTo('design');
      }
    });
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;


  }
]);

/*
                                                                     
                                                                     
                                                                     
         ##          ##           ##   ##      ###    ##          ## 
         ##          ##                ##     ## ##   ##          ## 
      ####    ###  #####   ###   ##   ##     ##  ## #####  # ##  ##  
     ## ##   ## ##  ##    #  ##  ##   ##     ##      ##    ###   ##  
     #  ##   #####  ##     ####  ##   ##    ##       ##    ##    ##  
    ##  #   ##     ##    ## ##  ##   ##     ##   #  ##    ##    ##   
    ## ##   ## ##  ##    ## ##  ##   ##     ##  ##  ##    ##    ##   
     ####    ###    ##    ## ## ##   ##      ####    ##   ##    ##   
                                                                     
                                                                     
*/



app.controller('detailCtrl', ['detailService', '$state', '$stateParams', '$scope', '$modal', '$log', '$filter', '$rootScope', 'notesService',

  function(detailService, $state, $stateParams, $scope, $modal, $filter, $log, $rootScope, notesService) {
    $scope.sub = $stateParams.id;
    $scope.detailError = false;
    $scope.detailRows = {};
    detailService.getDetail($scope.sub, $rootScope.notes).then(function(response) {

      $scope.detailRows = response.rows.rows;
      $scope.notesData.notes = response.notes;

      return $scope.detailRows;
    });

    $scope.notesData = {};
    $scope.notesData.notes = [];
    $scope.notesData.newNotes = [];
    console.log($scope.detailRows.selected);


    $scope.open = function(idx) {

      var modalInstance = $modal.open({
        templateUrl: 'static/partials/addDetail.html',
        controller: addFieldCtrl,
        resolve: {
          rows: function() {
            if (!$scope.selected) {
              $scope.selected = [];
            }

            $scope.detailRows['selected'] = $scope.selected;
            return $scope.detailRows[idx];
          }
        }
      });

      modalInstance.result.then(function(selectedItem) {
        $scope.selected = selectedItem;
      }, function() {
        $log.info('Modal dismissed at: ' + new Date());
      });

    };


    $scope.nameSplit = function(name) {
      var nameRaw = name.split(' ');
      var parsedName = nameRaw[0].nameRaw[1];
      return parsedName;
    };

    /*
        open function for adding notes

        */
    $scope.notesData.addNote = function(row) {


      $scope.notesData.newNotes.push({
        id: $scope.notesData.newNotes.length + 1,
        rows: $scope.detailRows,
        date: new Date(),
        user: $rootScope.user

      });
      console.log($scope.notesData.notes);
    }

    // delete existing note
    $scope.notesData.deleteNote = function(note) {
      console.log(note);
      notesService.deleteNote(note["_id"], $rootScope.user).then(function(response) {

        $state.go($state.current.name, $stateParams, {
          reload: true
        });
      });
    };
    $scope.$on('REMOVE_ROW', function(e, message) {
      console.log("Remove Row Event");
      console.log(e);
      console.log(message);
      $scope.notesData[message.array].splice(message.index, 1);

    });
    $scope.$on('ADD_NOTE', function(e, message) {
      var tempNotesObj = message.note;
      var idx = message.idx;
      var tempNoteRow = message.noteRow;
      notesService.writeNote(tempNotesObj).then(function(response) {
        console.log(response);

        $state.go($state.current.name, $stateParams, {
          reload: true
        });


      });

    });


  }
]);

/*
                                                               
                                                               
                    ##                          ##          ## 
                    ##                          ##          ## 
     ####    ###  #####   ###    ###      ### #####  # ##  ##  
     ## ##  ## ##  ##    ## ##  ## ##    ## #  ##    ###   ##  
     #  ## ##  ##  ##    #####   ##     ##     ##    ##    ##  
    ## ##  ##  ## ##    ##        ##    ##    ##    ##    ##   
    ## ##  ## ##  ##    ## ##  ## ##    ## ## ##    ##    ##   
    ## ##   ###    ##    ###    ###      ###   ##   ##    ##   
                                                               
                                                               
*/
app.controller('noteRowCtrl', ['$scope', '$rootScope', 'notesService', '$state', '$stateParams',
  function($scope, $rootScope, notesService, $state, $stateParams) {

    $scope.annotation;
    $scope.currentNote;
    $scope.save = function(idx) {
      console.log("starting save");
      console.log($scope.currentNote);
      if (!$scope.currentNote) {
        alert("fail");
        return;
      }
      //$scope.currentNote is currently a string -- it must be converted to a obj first
      $scope.currentNote = JSON.parse($scope.currentNote);

      tempNotesObj = {};
      tempNotesObj.rowData = $scope.currentNote;
      tempNotesObj.subID = $scope.currentNote.sub_id;
      tempNotesObj.activity = $scope.currentNote.activity_description;
      tempNotesObj.content = $scope.note.annotation;
      tempNotesObj.author = $scope.note.user.first_name + " " + $scope.note.user.last_name;

      tempNotesObj.safeID = $scope.note.user.safe_id;
      tempNotesObj.date = $scope.note.date;
      console.log(tempNotesObj);

      $scope.$emit('ADD_NOTE', {
        idx: idx,
        note: tempNotesObj,
        noteRow: $scope.note


      });
    }
    $scope.swapTasks = function(row) {
      if (row) {
        $scope.currentNote = row;
      }
    }
    $scope.rem = function(idx) {
      console.log(idx);

      $scope.$emit('REMOVE_ROW', {
        array: 'newNotes',
        index: idx
      });
    }
    $scope.saveEdits = function(note) {

      note.active = false;
      notesService.updateNote(note, $rootScope.user).then(function(response) {
        console.log(response);
        $state.transitionTo($state.current.name, $stateParams);
        alert("saved");
      });
    }

  }
]);


/*
                                                                           
                                                                           
                                                                           
                           ##    ##                         ##          ## 
                           ##                               ##          ## 
      ####    ###   # ## #####  ##    ###   ####      ### #####  # ##  ##  
      ## ##  #  ##  ###   ##    ##   ## #   ## ##    ## #  ##    ###   ##  
      #  ##   ####  ##    ##    ##  ##      #  ##   ##     ##    ##    ##  
     ##  #  ## ##  ##    ##    ##   ##     ##  #    ##    ##    ##    ##   
     ## ##  ## ##  ##    ##    ##   ## ##  ## ##    ## ## ##    ##    ##   
     ####    ## ## ##     ##   ##    ###   ####      ###   ##   ##    ##   
    ##                                    ##                               
    ##                                    ##                               
*/

app.controller('participantCtrl', ['participantService', '$stateParams', '$scope',
  function(participantService, $stateParams, $scope) {
    participantNameFull = $stateParams.id;
    a = participantNameFull.split("_");
    participantNameFirst = a[0];
    participantNameLast = a[1];
    participantService.getParticipant(participantNameFirst, participantNameLast).then(function(response) {
      $scope.participantRows = response.rows;
    });
  }
]);


/*
                                                                       
                                                                       
                                                                       
      ##    ##                         ##            ##      ##        
      ##    ##                         ##            ##      ##        
    #####  ####   # ##   ###    ###   ####    ###   ##    ####    ###  
     ##    ## ##  ###   ## ##  ## ##  ## ##  ## ##  ##   ## ##   ## ## 
     ##    #  ##  ##    #####   ##    #  ## ##  ##  ##   #  ##    ##   
    ##    ## ##  ##    ##        ##  ## ##  ##  ## ##   ##  #      ##  
    ##    ## ##  ##    ## ##  ## ##  ## ##  ## ##  ##   ## ##   ## ##  
     ##   ## ##  ##     ###    ###   ## ##   ###   ##    ####    ###   
                                                                       
                                                                       
*/
/*THRESHOLDS VIEW START*/


app.controller('thresholdsCtrl', ['$scope', '$http', '$window', '$timeout', 'thresholdServices',
  function($scope, $http, $window, $timeout, thresholdServices) {

    thresholdServices.getThresholds(function(data) {
      $scope.rows = data;
    });

    $scope.alerts = [];
    $scope.closeAlert = function(timeout) {

      $timeout(function() {
        $scope.alerts = [];
      }, timeout);
    };

    $scope.updateThresholds = function(row) {
      $http({
        url: '/update_thresholds',
        method: "POST",
        data: JSON.stringify({
          activity: row.activity,
          green_project: row.green_project,
          yellow_project: row.yellow_project
        }),
        headers: {
          'Content-Type': 'application/json'
        }
      }).success(function(data) {

        $scope.alerts.push({
          type: 'success',
          msg: data
        });
        $scope.closeAlert(2000);
      });
    };

    $scope.removeThreshold = function(row) {
      $http({
        url: '/remove_threshold',
        method: "POST",
        data: JSON.stringify({
          activity: row.activity,
          green_project: row.green_project,
          yellow_project: row.yellow_project
        }),
        headers: {
          'Content-Type': 'application/json'
        }
      }).success(function(data) {

        $scope.alerts.push({
          type: 'success',
          msg: data
        });
        $scope.closeAlert(2000);
        thresholdServices.getThresholds(data);
      });
    };


    $scope.thresholdData = {};
    $scope.thresholdData.newActivity = "Select Activity";
    $scope.thresholdData.newGreenPro = 0;
    $scope.thresholdData.newYellowPro = 0;
    thresholdServices.getActivitiesList(function(data) {
      $scope.activityList = data;
    });

    $scope.saveNewThreshold = function(thresholdData, key) {
      $http({
        url: '/save_new_threshold',
        method: "POST",
        data: JSON.stringify({
          activity: thresholdData.newActivity,
          green_project: thresholdData.newGreenPro,
          yellow_project: thresholdData.newYellowPro
        }),
        headers: {
          'Content-Type': 'application/json'
        }
      }).success(function(data) {
        $scope.alerts.push({
          type: 'success',
          msg: data
        });
        $scope.closeAlert(2000);
        thresholdServices.getThresholds(data);
        $scope.newThresholdForm.splice(key, 1);
        $scope.thresholdData.newActivity = "Select Activity";
        $scope.thresholdData.newGreenPro = 0;
        $scope.thresholdData.newYellowPro = 0;
      });
    };


  }
]);

/*THRESHOLDS VIEW END*/

/*
                                                          
                                                          
                                                          
                       ##                  ##          ## 
                                           ##          ## 
     # ## ##    ###   ##   ####      ### #####  # ##  ##  
     ## ## ##  #  ##  ##   ## ##    ## #  ##    ###   ##  
     ## ## ##   ####  ##   #  ##   ##     ##    ##    ##  
    ## ## ##  ## ##  ##   ## ##    ##    ##    ##    ##   
    ## ## ##  ## ##  ##   ## ##    ## ## ##    ##    ##   
    ## ## ##   ## ## ##   ## ##     ###   ##   ##    ##   
                                                          
                                                          
*/


app.controller("mainCtrl", function($scope, $rootScope, tempLiveSearchService, sharedProperties, userService, tempListService, $modal, $http, $timeout, $state, $stateParams) {
  console.log("main running");
  console.log(JSON.jsonify);
  $scope.selected = undefined;
  $scope.showFilter = false;
  $scope.predicate = '-firm_name';
  $scope.projNumOutput = 5;
  $scope.actNumOutput = 5;
  $scope.projLabel = "Late";
  $scope.actLabel = "Late";
  $scope.projectPieHasData = true;
  $scope.totalActCount = {};
  $scope.filterToggle = function() {
    $scope.showFilter = !$scope.showFilter;
    console.log($scope.storedActivityFilter, $scope.storedManagerFilter);

  };
  $scope.rows = [];
  $scope.pieActivityCount = {};
  $scope.outputPieActivity = [];
  $scope.outputPieProject = [];
  $scope.pieProjectCount = {};
  $scope.activity = "";
  $scope.thresholdsObj = {};
  $scope.activityFilters = "";
  $scope.managerFilters = [];
  $scope.productFilters = [];
  $scope.modifiedDate = "";

  /*    $scope.activity = "Design Prototype Due";
   */

  $scope.selectedOrg = capitalizeFirstLetter($state.current.name);
  $scope.storedActivityFilter;
  $scope.storedManagerFilter;
  console.log($rootScope.userData);

  $scope.exampleData = [{
    key: "One",
    y: 5
  }, {
    key: "Two",
    y: 2
  }, {
    key: "Three",
    y: 9
  }, {
    key: "Four",
    y: 7
  }, {
    key: "Five",
    y: 4
  }, {
    key: "Six",
    y: 3
  }, {
    key: "Seven",
    y: 9
  }];
  if ($rootScope.userData.pref_view) {
    if ($state.current.name == $rootScope.userData.pref_view.toLowerCase()) {
      if ($rootScope.userData.pref_act_filter) {
        $scope.storedActivityFilter = $rootScope.userData.pref_act_filter;
        console.log("set $scope.storedActivityFilter to " + $scope.storedActivityFilter);
      }
      if ($rootScope.userData.pref_manager_filter) {
        $scope.storedManagerFilter = $rootScope.userData.pref_manager_filter;
        console.log("set  $scope.storedManagerFilter to " + $scope.storedManagerFilter);
      }
    }
  } else {
    $scope.storedActivityFilter = "";
    $scope.storedManagerFilter = "";
  }
  if (sessionStorage.getItem($state.current.name + "ActivityFilter")) {
    console.log(sessionStorage.getItem($state.current.name + "ActivityFilter"));
    $scope.storedActivityFilter = sessionStorage.getItem($state.current.name + "ActivityFilter");
    console.log("set $scope.storedActivityFilter to " + $scope.storedActivityFilter);

  }
  if (sessionStorage.getItem($state.current.name + "ManagerFilter")) {
    $scope.storedManagerFilter = sessionStorage.getItem($state.current.name + "ManagerFilter");
    console.log("set  $scope.storedManagerFilter to " + $scope.storedManagerFilter);

  }


  tempLiveSearchService.getData($state.current.name).then(function(response) {
    // table data comes from $scope.rows
    // table data comes from $scope.rows
    $scope.rows = response.rows;
    tempArrA = [];
    tempArrP = [];
    $scope.thresholdsObj = response.thresholdsObj;
    $scope.managersList = response.managersList;
    $scope.untrackedData = response.untrackedData;
    $scope.activitiesList = response.activitiesList;
    $scope.totalActCount = response.threshActTotals;
    $scope.totalProjCount = response.threshProjTotals;
    $scope.productsList = response.productsList;
    console.log($scope.productsList);

    pieData = makeBothCharts("", $scope.thresholdsObj, $scope.totalActCount, $scope.totalProjCount, $scope.untrackedData);
    $scope.activityFilters = "";

    //console.log(pieData);
    $scope.outputPieActivity = pieData.activity;

    $scope.projColorFilter = "red";
    $scope.actColorFilter = "red";
    if (pieData.project == "untracked") {
      $scope.outputPieProject = [];
      $scope.projectPieHasData = false;

    } else {
      $scope.outputPieProject = pieData.project;
      $scope.projectPieHasData = true;
    }
    if ($scope.untrackedData) {
      $scope.projNumOutput = null;

    } else {
      $scope.projNumOutput = $scope.totalProjCount.totalProjRedCount;
    }
    $scope.actNumOutput = $scope.totalActCount.totalActRedCount;

    //receive updated color from hicharts directive
    $scope.$on('COLOR_CHANGE', function(e, message) {
      var convertedColor = capitalizeFirstLetter(message.color);
      $scope.projFilter = "";
      $scope.actFilter = "";
      switch (message.type) {
        case "project":

          $scope.$apply(function() {
            $scope.filterBy = "project";
            /*                                $scope.projColorFilter = message.color;
             */
            $scope.projLabel = message.label;
            console.log(message.color)
            if ($scope.activity === "") {
              $scope.projNumOutput = $scope.totalProjCount["totalProj" + convertedColor + "Count"];
              $scope.projFilter = capitalizeFirstLetter(message.color);
              console.log($scope.projColorFilter);
            } else {
              $scope.projNumOutput = $scope.thresholdsObj[$scope.activity]["project" + convertedColor + "Count"];
              $scope.projFilter = capitalizeFirstLetter(message.color);
            }
          });
          break;

        case "activity":
          //console.log(message.color + message.type);
          //console.log($scope.activity);
          $scope.$apply(function() {
            $scope.filterBy = "activity";
            $scope.actColorFilter = message.color;
            $scope.actLabel = message.label;
            /*$scope.actNumOutput = $scope.thresholdsObj[$scope.activity]["activity"+convertedColor+"Count"];*/
            if ($scope.activity === "") {
              $scope.actNumOutput = $scope.totalActCount["totalAct" + convertedColor + "Count"];
              $scope.actFilter = capitalizeFirstLetter(message.color);
              console.log($scope.actFilter);
            } else {
              $scope.actNumOutput = $scope.thresholdsObj[$scope.activity]["activity" + convertedColor + "Count"];
              $scope.actFilter = capitalizeFirstLetter(message.color);
              console.log($scope.actFilter);
            }
            //console.log( $scope.actNumOutput);
          });
          break;
        default:
          alert("unexpected emit message");
          break;
      }

    });

    if (_.contains($scope.activitiesList, $rootScope.userData.pref_act_filter)) {
      $scope.updateActivityFilters($rootScope.userData.pref_act_filter);
    }
    /* if (_.contains($scope.managersList, $rootScope.userData.pref_manager_filter)) {
            $scope.updateManagerFilters($rootScope.userData.pref_manager_filter);
        }*/
    // check session storage for most recent filters
    /*if ($scope.storedManagerFilter || $scope.storedActivityFilter) {

            if ($scope.storedActivityFilter) {
                $scope.updateActivityFilters($scope.storedActivityFilter);
            }
          if ($scope.storedManagerFilter != undefined && $scope.storedManagerFilter != null) {
                if ($scope.storedManagerFilter === "") {
                    $scope.updateManagerFilters("none");
                } else {
                    $scope.updateManagerFilters($scope.storedManagerFilter);
                }

            }
        }*/

  });

  $scope.updateActivityFilters = function(theActivity) {
    sessionStorage.setItem($state.current.name + "ActivityFilter", theActivity);
    if (theActivity != "none") {
      $scope.storedActivityFilter = theActivity;
      $scope.activityFilters = theActivity;
      pieData = makeBothCharts(theActivity, $scope.thresholdsObj);
      $scope.activity = theActivity;
      //console.log(pieData.activity);
      if (pieData != "no data") {
        $scope.outputPieActivity = pieData.activity;
        /*$scope.outputPieProject = pieData.project;*/
        if (pieData.project == "untracked") {
          $scope.outputPieProject = [];
          $scope.projectPieHasData = false;
        } else {
          $scope.outputPieProject = pieData.project;
          $scope.projectPieHasData = true;
        }
        $scope.filterBy = "";
        if ($scope.thresholdsObj[theActivity].activityRedCount != 0) {
          console.log("we've got red");
          $scope.actColorFilter = "red";
          $scope.actLable = $scope.actLabel = "Late";
          $scope.actNumOutput = $scope.thresholdsObj[theActivity].activityRedCount;
        } else if ($scope.thresholdsObj[theActivity].activityYellowCount != 0) {
          console.log("we've got yellow");
          $scope.actColorFilter = "yellow";
          $scope.actLable = $scope.actLabel = "Caution";
          $scope.actNumOutput = $scope.thresholdsObj[theActivity].activityYellowCount;
        } else {
          console.log("we've got green");
          $scope.actColorFilter = "green";
          $scope.actLable = $scope.actLabel = "On Time";
          $scope.actNumOutput = $scope.thresholdsObj[theActivity].activityGreenCount;
        }
        if ($scope.thresholdsObj[theActivity].projectRedCount != 0) {
          console.log("we've got red");
          $scope.projColorFilter = "red";
          $scope.projLable = $scope.projLabel = "Late";
          $scope.projNumOutput = $scope.thresholdsObj[theActivity].projectRedCount;
        } else if ($scope.thresholdsObj[theActivity].projectYellowCount != 0) {
          console.log("we've got yellow");
          $scope.projColorFilter = "yellow";
          $scope.projLable = $scope.projLabel = "Caution";
          $scope.projNumOutput = $scope.thresholdsObj[theActivity].projectYellowCount;
        } else {
          console.log("we've got green");
          $scope.projColorFilter = "green";
          $scope.projLable = $scope.projLabel = "On Time";
          $scope.projNumOutput = $scope.thresholdsObj[theActivity].projectGreenCount;
        }


        $scope.noPieData = false;
      } else {
        alert("no data for this activity");
        $scope.noPieData = true;
      }
    } else {

      //$scope.activityFilters = " ";
      pieData = makeBothCharts("", $scope.thresholdsObj, $scope.totalActCount, $scope.totalProjCount, $scope.untrackedData);
      $scope.activityFilters = "";
      $scope.outputPieActivity = pieData.activity;



      $scope.actNumOutput = $scope.totalActCount.totalActRedCount;

      $scope.projColorFilter = "red";
      $scope.actColorFilter = "red";
      $scope.storedActivityFilter = "";

    }

  };

  $scope.updateManagerFilters = function(theManager, idx) {



    //sessionStorage.setItem($state.current.name + "ManagerFilter", theManager);
    if (theManager != "none") {
      if (!_.contains($scope.managerFilters, theManager)) {
        $scope.managerFilters.push(theManager);
      } else {
        for (var i = 0; i < $scope.managerFilters.length; i++) {
          if ($scope.managerFilters[i] === theManager) {
            $scope.managerFilters.splice(i, 1);
            break;
          }
        }
      }

      //$scope.storedManagerFilter = theManager;
    } else {
      $scope.managerFilters = [];
      //$scope.storedManagerFilter = "";
    }

  };
  $scope.updateProductFilters = function(theProduct) {
    // will reimplement when all multi-selects (checkboxes) are in place
    /*console.log(theProduct);

        sessionStorage.setItem($state.current.name + "ProductFilter", theProduct);
        if (theProduct != "none") {
            $scope.productFilters = theProduct;
            $scope.storedProductFilter = theProduct;
        } else {

            $scope.productFilters = "";
            $scope.storedProductFilter = "";
        }*/
    if (theProduct != "none") {
      if (!_.contains($scope.productFilters, theProduct)) {
        $scope.productFilters.push(theProduct);
      } else {
        for (var i = 0; i < $scope.managerFilters.length; i++) {
          if ($scope.productFilters[i] === theProduct) {
            $scope.productFilters.splice(i, 1);
            break;
          }
        }
      }

      //$scope.storedProductFilter = theProduct;
    } else {

      $scope.productFilters = [];
      //$scope.storedManagerFilter = "";
    }
    sessionStorage.setItem($state.current.name + "ProductFilter", JSON.stringify($scope.productFilters));

  };
  $scope.filterManagers = function(item) {

    if ($scope.managerFilters.length < 1) {
      return true;
    }
    if (_.contains($scope.managerFilters, item.staff_manager)) {

      return true;

    } else {

      return false;
    }
  }
  $scope.filterProducts = function(item) {
    console.log(item.product);
    if ($scope.productFilters.length < 1) {
      return true;
    }
    if (_.contains($scope.productFilters, item.short_product)) {

      return true;

    } else {

      return false;
    }
  }
  /*TEMPORARY CLICK TO REFRESH BUTTON WITH ALERT*/
  $scope.alerts = [];
  $scope.closeAlert = function(timeout) {

    $timeout(function() {
      $scope.alerts = [];
    }, timeout);
  };
  /* $scope.close = function(){
            $scope.alerts= [];
             $scope.loading = false;
        };*/

  $scope.updateData = function() {
    $scope.loading = true;
    $scope.warningMsg = false;
    $http.get('/update_data').success(function(data) {
      var currentOrg = $state.current.name;
      var currentOrgParsed = capitalizeFirstLetter(currentOrg);
      if (data == 'Whoops! Data was not updated!') {
        $scope.alerts.push({
          type: 'danger',
          msg: data
        });
        $scope.loading = false;
        $scope.warningMsg = true;

      } else {
        $scope.alerts.push({
          type: 'success',
          msg: data
        });
        sharedProperties.getRows(currentOrgParsed).then(function(response) {
          // table data comes from $scope.rows
          $scope.rows = response.rows;
        });
        $scope.loading = false;
        $scope.closeAlert(2000);
      }


    });
  };


  $scope.focusFilter = function(actColor, projColor) {
    var returnResult = "";
    $scope.proColorFilter = "";
    $scope.actColorFilter = "";
    switch ($scope.filterBy) {

      case "activity":
        actColorFilter = actColor;
        console.log(actColor)
        return returnResult;
        break;
      case "project":
        proColorFilter = projColor;
        console.log(returnResult)
        return returnResult;
        break;
      default:

        return returnResult;
        break;
    }


  }
  $scope.clearColorFilters = function(ageType) {

    if (ageType == "project") {
      $scope.projFilter = "";
      $scope.filterBy = "";
      console.log("Clearing project color filter");
    } else if (ageType = "activity") {
      $scope.actFilter = "";
      $scope.filterBy = "";
      console.log("Clearing activity color filter");
    } else {
      //console.log("unexpected age type");
    }

  }

  $scope.openUnassigned = function() {
    var modalInstance = $modal.open({
      templateUrl: 'static/partials/unassigned.html',
      controller: unassignedCtrl,
      resolve: {
        rows: function() {
          //console.log("returning rows to modal");
          return $scope.rows;
        }
      }
    });

  };


  $scope.cancel = function() {
    $modalInstance.close('cancel');
  };



});


var unassignedCtrl = function($scope, $modalInstance, $filter, rows) {
  $scope.rows = rows;
  $scope.unassignedFilter = function(row) {
    return row.participant_last_name === "Unassigned" && row.activity_status === "Red" || row.participant_last_name === "Unassigned" && row.project_color === "red";
  };

  $scope.ok = function() {
    $modalInstance.close();

  };
  $modalInstance.cancel = function() {
    $modalInstance.dismiss('cancel');
  };

};
/*
                                                        
                                                        
                                                        
                    ##    ##    ##                      
                    ##    ##                            
      ###    ###  ##### #####  ##   ####    ####   ###  
     ## ##  ## ##  ##    ##    ##   ## ##  ## ##  ## ## 
      ##    #####  ##    ##    ##   #  ##  #  ##   ##   
       ##  ##     ##    ##    ##   ## ##  ##  #     ##  
    ## ##  ## ##  ##    ##    ##   ## ##  ## ##  ## ##  
     ###    ###    ##    ##   ##   ## ##   ####   ###   
                                            ##          
                                          ###           
                                         
                                         
                                         
                          ##          ## 
                          ##          ## 
     # ## ##    ###    ####    ###   ##  
     ## ## ##  ## ##  ## ##   #  ##  ##  
     ## ## ## ##  ##  #  ##    ####  ##  
    ## ## ##  ##  ## ##  #   ## ##  ##   
    ## ## ##  ## ##  ## ##   ## ##  ##   
    ## ## ##   ###    ####    ## ## ##   
                                         

*/
var settingsCtrl = function($scope, $rootScope, $modalInstance, user, userData, tempListService, userService, $state, $stateParams) {

  $scope.user = user;
  $scope.userData = $rootScope.userData;
  $scope.settingsOrg;
  $scope.orgManagers = [];
  $scope.orgActivities = [];
  $scope.setFlag = false;
  $scope.orgChoices = [

    {
      "name": "Design",
      "value": "Design"
    }, {
      "name": "Development",
      "value": "Development"
    }

  ];

  // if there is a pref view - set it
  if ($rootScope.userData.pref_view != "" || $rootScope.userData.pref_view != undefined) {
    for (var i = 0; i < $scope.orgChoices.length; i++) {

      if ($rootScope.userData.pref_view == $scope.orgChoices[i].name) {
        $scope.settingsOrg = $scope.orgChoices[i];
        $scope.setFlag = true;
        break;
      }
    }

  }



  $scope.listsLoaded = false;
  var lists = [];
  var checkLists = function() {

    if (lists.length >= 2) {
      $scope.listsLoaded = true;


    }
    // all lists loaded, now check for matches
    if ($rootScope.userData.pref_manager_filter != null || $rootScope.userData.pref_manager_filter != undefined && $rootScope.userData.pref_act_filter != null || $rootScope.userData.pref_act_filter !== undefined) {

      $scope.settingsManager = $rootScope.userData.pref_manager_filter;

      $scope.settingsActivity = $rootScope.userData.pref_act_filter;
      console.log($scope.settingsManager, $scope.settingsActivity);

    }

  }


  $scope.setLists = function() {
    console.log("we're setting lists!");
    lists = [];

    $scope.orgManagers = [];
    $scope.orgActivities = [];
    console.log(this);
    //using this because of the child scope
    tempListService.getList("manager", this.settingsOrg.name).then(function(response) {
      $scope.orgManagers = response.data;

      lists.push(response.data);
      checkLists();

    });
    tempListService.getList("activity", this.settingsOrg.name).then(function(response) {
      $scope.orgActivities = response.data;
      lists.push(response.data.list);
      checkLists();
    });
  };

  if ($rootScope.userData.pref_manager_filter != null || $rootScope.userData.pref_manager_filter != undefined && $rootScope.userData.pref_act_filter != null || $rootScope.userData.pref_act_filter !== undefined) {
    $scope.listsLoaded = true;
    $scope.setLists();
    console.log("this is true");


  }

  $scope.ok = function() {
    if (!this.settingsOrg.value || this.settingsOrg.name === "-- choose a default view --") {
      alert("you must set a default organization to save user preferences");

    }
    var formData = {
      "org": this.settingsOrg.value,
      "activity": this.settingsActivity,
      "manager": this.settingsManager
    };

    userService.setUserData(formData).then(function(response) {

      $rootScope.userData = response;



      $modalInstance.close();
      $state.go($rootScope.userData.pref_view.toLowerCase(), $stateParams, {
        reload: true
      });
    });


  };
  $scope.cancel = function() {
    $modalInstance.dismiss('cancel');
  };
  $scope.resetAll = function() {
    //remove all preferences
    userService.resetUserPrefs($rootScope.user).then(function(response) {
      console.log(response);
      $rootScope.userData = response;
      $scope.userData = response;
      $scope.setFlag = false;
      $scope.settingsOrg = {
        "name": "--Select One --",
        "value": 0
      }
      $scope.listsLoaded = false;
      lists = [];
    })

  }

}



/*var viewCtrl = app.controller("viewCtrl", function($scope, $route) {

});*/
/*
                                                              
                                                              
                                                              
                    ##                         ##          ## 
                    ##                         ##          ## 
      ###   ## ## #####   ###   # ##     ### #####  # ##  ##  
     ## ##  ## ##  ##    ## ##  ###     ## #  ##    ###   ##  
    ##  ## ##  ##  ##    #####  ##     ##     ##    ##    ##  
    ##  ## ## ##  ##    ##     ##      ##    ##    ##    ##   
    ## ##  ## ##  ##    ## ##  ##      ## ## ##    ##    ##   
     ###    ## #   ##    ###   ##       ###   ##   ##    ##   
                                                              
                                                              
*/

var outerCtrl = app.controller("outerCtrl", function($modal, $scope, $rootScope, $stateParams, userService, tempListService, tempLiveSearchService, $state, notesService) {

  $scope.user = {};
  $scope.userData = {};
  $scope.pref = {};
  $rootScope.notes = {};
  notesService.readNotes().then(function(response) {
    $rootScope.notes = response;
  });


  userService.getUser().then(function(response) {

    $rootScope.user = response;

    /*        if ($rootScope.user.first_name === "Brian" && $rootScope.user.last_name == "Rogstad") {
            $.getScript('http://www.cornify.com/js/cornify.js', function() {
                cornify_add();
                $(document).keydown(cornify_add);
                $(document).mousedown(cornify_add);
            });
        }*/
  });

  tempLiveSearchService.getDateModified().success(function(response) {
    modifiedDateString = response;
    $scope.IsoDateModified = Date.parse(modifiedDateString);
    console.log($scope.IsoDateModified);
  });


  $scope.openSettings = function() {

    $scope.loading = true;

    userService.getUserData().then(function(response) {
      $rootScope.userData = response;

      $scope.loading = false;
      var modalInstance = $modal.open({
        templateUrl: 'static/partials/settings.html',
        controller: settingsCtrl,
        resolve: {
          userData: function() {
            return $scope.userData;
          },
          user: function() {
            return $rootScope.user;
          }

        }
      });
    })

  };

  // set current state to session storage for page refreshes
  $rootScope.$on('$stateChangeSuccess',
    function(event) {
      paramsString = JSON.stringify($stateParams);
      sessionStorage.setItem("lastViewed", $state.current.name);

      sessionStorage.setItem("lastViewedParams", paramsString);


    });

});


var addFieldCtrl = function($scope, $modalInstance, $filter, rows) {
  $scope.rows = rows;
  //Grab all keys from rows and create an array
  $scope.keyList = [];
  angular.forEach($scope.rows, function(value, key) {
    $scope.keyList.push(key);
  });

  console.log($scope.rows.selected);

  if ($scope.rows.selected) {
    $scope.selection = $scope.rows.selected;
  } else {
    $scope.selection = [];
  }

  console.log("butter", $scope.selection)

  $scope.toggleSelection = function toggleSelection(item) {
    var idx = $scope.selection.indexOf(item);

    if (idx > -1) {
      $scope.selection.splice(idx, 1);
    } else {
      $scope.selection.push(item);
    }
    /*console.log(item);
        $scope.selectedFields.push(item);*/
  };

  $scope.ok = function() {
    $modalInstance.close($scope.selection);

  };
  $scope.cancel = function() {
    $modalInstance.dismiss('cancel');
  };


}